%\fixme{Placeholder. Note that this chapter will be turned into a publication. Expect to complete this chapter in a few hours. It's not a difficult or long chapter to write anyway.}

\textbf{Note:} This chapter will be turned into a short author publication:
\begin{adaption}
	\begin{centering}
		``Improving the ranking statistic assignment of the GstLAL search pipeline with the inclusion of iDQ data-quality information'', In preparation.}
        \end{centering}
\end{adaption}


\section{Introduction and Overview}
\label{sec: idq_intro_and_overview}
One of the GstLAL search pipeline's main development goals is to improve its search sensitivity towards gravitational waves from compact binary coalescences. As we have briefly mentioned in the previous chapter, the lack of time dependence of the $\rho$-$\xi^2$
histograms used by the GstLAL pipeline to assign ranking statistics can potentially cause GstLAL to miss gravitational-wave signals, leading to a loss in search sensitivity.

In this chapter, I describe a method that utilizes statistical data quality from the iDQ pipeline \cite{Godwin:2020weu,Essick:2020qpo}
information as a workaround to the stated problem above.
This chapter is arranged as follows: 
In Section \ref{sec: idq_background}, I will briefly recap the computation of the $P(\rho, \xi^2 | \text{noise})$ portion 
for the calculation of the likelihood ratio for the GstLAL pipeline and 
give a short introduction to iDQ, a machine-learning-based data quality pipeline. 
In Section \ref{sec: idq_method}, I will outline the proposed method to incorporate information from the iDQ pipeline 
into GstLAL's likelihood ratio calculation. 
Finally, in Section \ref{sec: idq_expected_results}, I will briefly discuss the expected results with the successful implementation of the proposed method.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Background}
\label{sec: idq_background}

In this section, I will briefly recap how GstLAL computes the likelihood ratio for triggers, explicitly focusing on the $P(\vec\rho, \vec\xi^2 | \vec\theta, \mathrm{noise})$ term.
I will briefly introduce the iDQ data quality pipeline \cite{Godwin:2020weu,Essick:2020qpo}.

\subsection{\underline{GstLAL's likelihood ratio statistic}}
In Chapter \ref{Chapter: introduction_gstlal}, we explained how GstLAL assigns to each trigger a likelihood ratio defined by
\begin{align}
	\mathcal{L} = \frac{P(\vec{D}_H,\vec{O},\vec\rho,\vec\xi^2,\left[\vec{\Delta t}, \vec{\Delta \phi}\right]|\vec\theta,\mathbf{signal})}{P(\vec{D}_H,\vec{O},\vec\rho,\vec\xi^2,\left[\vec{\Delta t}, \vec{\Delta \phi}\right]|\vec\theta,\mathbf{noise})}\cdot\frac{P(\vec\theta|\mathbf{signal})}{P(\vec\theta|\mathbf{noise})},
\end{align}
where
(1) $\vec{D}_H$ are the horizon distances of the detectors (a measure of the detector sensitivity),
(2) $\vec{O}$ is the set of detectors that registered the coincident trigger,
(3) $\vec\rho$ are the recorded detector SNRs for the trigger at each participating detector,
(4) $\vec\xi^2$ are the auto-correlation-based consistency test values evaluated for each of the participating detectors,
(5) $\left[\vec{\Delta t}, \vec{\Delta \phi} \right]$ are the sets of differences in end times $\Delta t$ and coalescence phases $\Delta phi$
between pairs of detectors within the set of participating detectors and
(6) $\vec\theta$ is the template parameters associated with the trigger.

The likelihood ratio can be factorized into products of smaller terms \cite{2017PhRvD..95d2001M,2015arXiv150404632C}.
This subsection will focus on the term $P(\vec\rho, \vec\xi^2 | \vec\theta, \mathrm{noise})$. 
This term gives the probability of obtaining a noise trigger with SNRs $\vec\rho$ and $\xi^2$. 
To compute this probability, GstLAL populates 2D histograms in the $\rho$-$\xi^2$ space for each participating detector 
with non-coincident triggers, assuming they represent triggers originating from noise. The histograms are normalized and smoothed with a Gaussian smoothing kernel to produce an approximated $P(\vec\rho, \vec\xi^2 | \vec\theta, \mathrm{noise})$
for assigning likelihood ratios for triggers. As noted in the previous chapter and \cite{2017PhRvD..95d2001M}, these histograms are constructed cumulatively throughout the data being analyzed, and no time dependence is tracked.

Consider the following scenario: The data quality is generally ``bad'' (i.e., the data are ``noisy'' and potentially contain numerous glitches) over a certain period of analysis time. We perform a search across the data that includes the aforementioned period of data with the GstLAL search pipeline. Glitches and noise in the noisy period of data get registered as noise triggers. These triggers, in particular those corresponding to glitches, may have SNRs $\rho$'s and $\xi^2$ very different from those of noise triggers from stationary and Gaussian noise. These triggers will enter the $\rho$-$\xi^2$ histograms and can potentially ``pollute''
\footnote{
	Generally speaking, the probability distribution is ``polluted'' if it is broadened by
	extreme data.	
} 
the expected probability distribution of $\rho$ and $\xi^2$ for noise triggers due to their extreme values.
At some later time, when the data becomes less ``noisy'', a near-threshold, low-amplitude gravitational-wave signal may still get registered as a trigger. However, since the $\rho$-$\xi^2$ histogram for background triggers has been polluted by earlier noisy data, triggers with weaker SNRs may potentially be assigned a smaller likelihood ratio and significance, causing the pipeline to miss the signal potentially. 
In Section \ref{sec: idq_method}, we propose a method to potentially resolve this problem by modifying the likelihood ratio statistic to consider data quality around the time of the trigger when assigning ranking statistics.


\subsection{\underline{The iDQ data quality pipeline: A brief introduction}}
iDQ is a machine-learning-based statistical inference pipeline that generates probabilistic information about the data quality in quasi-real time \cite{Essick:2020qpo,Godwin:2020weu}. The iDQ pipeline has been operating since LVK's first observation run. The main goal of the iDQ pipeline is to automatically identify potential non-Gaussian noise (glitches) in the data and labelled times in the data up to subsecond intervals as likely to contain a glitch in low latency. Details about the formalism, setup of the iDQ pipeline (including the training and training data generation), and effectiveness evaluation of the iDQ pipeline are all out of the scope of this thesis. Related information, however, can be found in \cite{Essick:2020qpo}.
\begin{figure}[hbt!]
\centering
\includegraphics[width=\textwidth]{idq_timeseries_example.png}
\caption{
	An example timeseries of log-likelihood estimate $\ln \mathcal{L}_\text{iDQ}$ of the presence of non-Gaussian noise in the data output from the iDQ pipeline. The timeseries covers roughly $5000$s of data (i.e.  $\approx$ 1.3 hours). Note that $\ln \mathcal{L}_\text{iDQ}$ has a value of $\approx 9$ most of time, but occasionally it can increase to $70$ or more (e.g. see the peak at GPS times $\approx 2600$s). These peaks represent times the iDQ pipeline believes glitches are present in the data. 
}
\label{fig:idq_example}
\index{figures}
\end{figure}
One of the data products output from the iDQ pipeline is a log-likelihood estimate $\ln \mathcal{L}_\text{iDQ}$ of the presence of non-Gaussian noise in the data as a function of time (i.e. a timeseries. See Figure \ref{fig:idq_example}, for example). For practical reasons, as suggested in \cite{Godwin:2020weu}, $\ln  \mathcal{L}_\text{iDQ}$ is renormalized to control the impact on search pipelines better when being incorporated into ranking statistic calculations.
For a given duration of data being analyzed, the raw $\ln \mathcal{L}_\text{iDQ}$ timeseries
sampled at $128$ Hz is aggregated and maximized over a $\pm 1$ second time window. The
resulting values are then used to evaluate the renormalized iDQ log-likelihood defined by
\begin{align}
	\ln \hat{\mathcal{L}}_\text{iDQ} = 
	\begin{cases}
		0, P<P_\text{min}, \\
		\ln \frac{P}{100-P}, P_\text{min} \leq P < P_\text{max},\\
		15, P \geq P_\text{max}
	\end{cases},
\end{align}
where $P_\text{min} = 50$, and
\begin{align}
	P_\text{max} = \frac{100}{1 + \exp\left[-\ln \hat{\mathcal{L}}_\text{iDQ}^\text{upper}\right]} = \frac{100}{1 + e^{-15}},
\end{align}
with $\ln \hat{\mathcal{L}}_\text{iDQ}^\text{upper}$ being the predefined upper limit of the renormalized iDQ log-likelihood,
which is set to be $15$.
$P$ is the percentile of $ln \mathcal{L}_\text{iDQ}$ values across the whole timeseries.
$P_\text{min}$ and $P_\text{max}$ are chosen accordingly to limit the renormalized $\ln \mathcal{L}_\text{iDQ}$
to a range between $0$ and $15$.
Values below the $50^\text{th}$ percentile are assigned a renormalized iDQ log-likelihood of $0$.
The higher $\ln \hat{\mathcal{L}}_\text{iDQ}$ is, the more likely there exists
non-Gaussian noise in the data.
Interested readers are referred to \cite{Essick:2020qpo,Godwin:2020weu} for details.

\section{Proposed method}
\label{sec: idq_method}
\begin{figure}[hbt!]
\centering
\includegraphics[width=\textwidth]{3d_snr_chisq_idq_histogram_scheme}
\caption{
	The proposed scheme on modifying the original $P(\rho, \xi^2 | \text{noise})$ term in GstLAL's likelihood ratio calculations.
	We propose promoting the 2D $\rho$-$\xi^2$ histogram for noise triggers to 3D histograms such that the renormalized iDQ log-likelihood
	$\ln \hat{\mathcal{L}}_\text{iDQ}$ as an additional parameter. 
	In the figure, the proposed 3D histogram is represented by a series of slices of 2D $\rho$-$\xi^2$ histograms along the $iDQ$ dimension. 
	GstLAL will then populate the extended 3D histograms for each detector
	with noise triggers, according to the data quality around the time of each trigger informed by $\ln \hat{\mathcal{L}}_\text{iDQ}$.
	Then, as usual, the histograms will be normalized and smoothed with a Gaussian smoothing kernel. The end product will be a three-dimensional
	probability density function $P(\rho, \xi^2, \ln \hat{\mathcal{L}}_\text{iDQ} | \text{noise})$, which can be used to replace the
	original $P(\rho, \xi^2 | \text{noise})$ term in the likelihood ratio calculation in GstLAL.
}
\label{fig:idq_proposed_scheme}
\index{figures}
\end{figure}

Recall that the problem with the current implementation of the $P(\rho, \xi^2|\text{noise})$ term in GstLAL's log-likelihood ratio
calculation is that non-Gaussianity in data can potentially ``pollute'' the cumulative $\rho$-$\xi^2$ histograms for background noise
which are used to approximate $P(\rho, \xi^2|\text{noise})$. The approximated, ``polluted'' $P(\rho, \xi^2|\text{noise})$ function 
can lead to unnecessary reductions in the assigned log-likelihood ratio for triggers that are found in times where
data quality is relatively good. This may lead to signal loss and a decrease in the search sensitivity of the pipeline.

Figure \ref{fig:idq_proposed_scheme} shows a schematic overview of our proposed method to resolve the problem potentially.
We propose to promote the current 2D probability density function $P(\rho, \xi^2 | \text{noise})$ to a 3D probability density
function $P(\rho, \xi^2, \ln \hat{\mathcal{L}}_\text{iDQ} | \text{noise})$ that includes the renormalized iDQ log-likelihood
$\ln \hat{\mathcal{L}}_\text{iDQ}$ as an additional paramter. The middle part of Figure \ref{fig:idq_proposed_scheme} shows two
``slices'' of the proposed 3D probability density function, i.e. $P(\rho, \xi^2, \ln \hat{\mathcal{L}}_\text{iDQ} | \text{noise})$
at fixed iDQ log-likelihood values. The top and bottom histograms illustrate how the projected 2D probability density function 
will look like for low $\ln \hat{\mathcal{L}}_\text{iDQ}$ (corresponding to times where data quality is good) and high
$\ln \hat{\mathcal{L}}_\text{iDQ}$ (corresponding to times where the data quality is bad) respectively. One will expect that the
projection of the 3D probability density function $P(\rho, \xi^2, \ln \hat{\mathcal{L}}_\text{iDQ} | \text{noise})$ to span
a larger region (i.e. the distribution is broader) in the $\rho$-$\xi^2$ space because it is populated and ``polluted'' with 
triggers coming from non-Gaussian features in the data (e.g. glitches).

In the modified scheme, GstLAL will set up 3D histograms in the SNR $\rho$-$\xi^2$-$\ln \hat{\mathcal{L}}_\text{iDQ}$ parameter
space per detector, populated by noise triggers collected during the filtering process. These histograms, as usual, will then
be normalized and smoothed with a Gaussian smoothing kernel to approximate the three-dimensional probability density function 
$P(\rho, \xi^2, \ln \hat{\mathcal{L}}_\text{iDQ} | \text{noise})$, which is then used to assign likelihood ratios to signal
triggers.

\section{Expected results}
\label{sec: idq_expected_results}
Because of various reasons, implementing and testing the aforementioned method remains an ongoing work. 
I will briefly discuss the expected results.
\begin{figure}[hbt!]
\centering
\includegraphics[width=\textwidth]{idq_expected_result}
\caption{
	An illustration showing how the proposed method can improve the search sensitivity of GstLAL.
	Suppose we have a signal trigger found in a detector when the data quality is good.
	A green star in the figure represents the signal trigger.
	In the current GstLAL framework, since data quality information is not used, the 3D $\rho$-$\xi^2$-$\ln \hat{\mathcal{L}}_\text{iDQ}$
	histograms (middle) for noise triggers are essentially ``marginalized'' over the data quality (iDQ) dimension, resulting in the usual 
	2D $\rho$-$\xi^2$ histogram on the left. As discussed before, non-Gaussianity (e.g. glitches) in the data can pollute the histograms and effectively
	broaden the probability distribution $P(\rho, \xi^2 | \text{noise})$. As shown in the figure, the trigger found during times with good 
	data quality lies in the region of the marginalized probability distribution $P(\rho, \xi^2 | \text{noise})$ with high probability density,
	that is polluted by noise triggers associated with non-Gaussian noise in the data.
	Essentially, this means that $P(\rho, \xi^2 | \text{noise})$ for this trigger will be unnecessarily high, resulting in a lower log-likelihood
	ratio and significance being assigned to it.
	In the proposed method, on the other hand, the trigger found in times with good data quality effectively has its log-likelihood
	ratios assigned using a histogram that suits the data quality around the time of the trigger (e.g. right upper histogram).
	As we can see, the trigger does not live within the probability distribution $P(\rho, \xi^2 | \text{noise})$ for noise triggers
	coming from times with good data quality, and it will be assigned a lower $P(\rho, \xi^2 | \text{noise})$ and hence higher log-likelihood
	ratio and significance.
	This helps to prevent unnecessary down-ranking of triggers, which may lead to potential loss of gravitational-wave signals.
}
\label{fig:idq_expected_result}
\index{figures}
\end{figure}
Figure \ref{fig:idq_expected_result} illustrates how the proposed method can potentially improve the search sensitivity of GstLAL.
Suppose we have a signal trigger found when the data quality is good.
In the figure, the example trigger is represented by a green star.
Under GstLAL's current framework, since data quality information is not considered, 
the 3D $\rho$-$\xi^2$-$\ln \hat{\mathcal{L}}_\text{iDQ}$ histograms (in the middle of the figure) for noise triggers 
are essentially ``marginalized'' over the data quality (iDQ) dimension, 
resulting in the usual 2D $\rho$-$\xi^2$ histogram shown on the left. 
As discussed in Section \ref{sec: idq_background}, 
extreme noise triggers originating from non-Gaussianity (e.g. glitches) in the data can 
``pollute'' the histograms and effectively broaden the probability distribution $P(\rho, \xi^2 | \text{noise})$. 
As shown in the left histogram in the figure, the trigger found during times with good data quality 
lies in the region of the marginalized probability distribution $P(\rho, \xi^2 | \text{noise})$ 
with high probability density that is ``polluted'' by extreme noise triggers 
associated with non-Gaussian noise in the data. 
Essentially, this means that $P(\rho, \xi^2 | \text{noise})$ for this trigger will be unnecessarily high, 
resulting in a lower log-likelihood ratio and significance being assigned to it.
The trigger with a low assigned significance can potentially be buried in the noise background and not 
being identified as a gravitational wave signal, leading to a reduction in the search sensitivity of the pipeline.

In the proposed method, the trigger found in times with good data quality effectively has their log-likelihood ratios assigned using a histogram that suits the data quality around the time of 
the trigger (e.g. right upper histogram). As we can see, the trigger does not live within the probability 
distribution $P(\rho, \xi^2 | \text{noise})$ for noise triggers coming from times with good data quality, 
and it will be assigned a lower $P(\rho, \xi^2 | \text{noise})$ and hence higher log-likelihood ratio 
and significance. This helps to prevent unnecessary down-ranking of triggers that lead to potential 
loss of gravitational-wave signals.

%\section{Conclusion and future work}
%\label{sec: idq_conclusion_and_future_work}


%\begin{figure}[hbt!]
%\centering
%\includegraphics[width=\textwidth]{cross_polarization.png}
%\caption{Cross Polarization.}\label{fig:cross_polarization}
%\index{figures}
%\end{figure}

