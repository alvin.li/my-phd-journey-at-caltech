\section{Summary of Thesis}
The detection of gravitational waves by the LIGO-Virgo-KAGRA (LVK) collaboration has opened 
a new window for us to study the universe. 
We have detected gravitational waves from compact binary coalescences, which are mergers of binary compact objects: black holes and/or neutron stars. 
To continue pushing the boundaries to explore new science via gravitational waves,
we need to enhance both the hardware,
e.g. improving the gravitational-wave detectors' sensitivities and building more ground-based 
detectors to expand the detector network; 
as well as software, e.g. developing better search pipelines for gravitational waves and 
data analysis techniques to probe gravitational-wave sources with the broadest range of 
parameters and from the furthest distances.
With these efforts, we aim to populate our catalog of gravitational-wave events maximally
not restricted to only known and confirmed sources like compact binary coalescences, but
also, other types of gravitational waves, including 
gravitational-wave bursts from core-collapse supernovae, 
continuous gravitational waves from spinning non-axisymmetric neutron stars,
and stochastic gravitational-wave background from sources that are not individually resolvable. 
These detections allow us to explore many essential and exciting topics in astrophysics
that can foster our understanding of our universe's fundamental laws and evolution.
For instance, gravitational waves can be deflected by curvature in spacetime caused by massive 
intervening objects like galaxies and galaxy clusters as electromagnetic (EM) waves are, 
an effect known as gravitational lensing. 
Successful detection of lensed gravitational waves will be a tremendous discovery for the 
(astro)physics community. 
It will also be a pivotal pathway to study the higher-redshift early universe and, 
ultimately, cosmic evolution. 

In this thesis, I focus on improving the search sensitivity of the matched-filtering
based search pipeline GstLAL, and searching for gravitationally-lensed gravitational waves.
Here, I will briefly summarize the content of my thesis.
Beginning with Chapter \ref{Chapter: Intro_to_GW}, I gave a very brief overview of the key
ideas and concepts in General Relativity, followed by the standard derivations of linearized
gravity in the weak-field limit, and how gravitational waves emerge from the resulting Einstein's
field equations. I also briefly introduced the Advanced LIGO gravitational-wave detectors
and derived the antenna pattern functions associated with the LVK collaborations' L-shaped detectors.
Then, in Chapter \ref{Chapter: Introduction_to_DA_of_GW}, I gave a top-level review of the current 
framework of the LVK collaboration to search for gravitational waves from compact binary coalescences,
including data calibration, searches for gravitational waves in calibrated data, and Bayesian
parameter estimation analysis for gravitational-wave events. 
In Chapter \ref{Chapter: introduction_gstlal}, I explained in detail how matched-filtering pipelines
search for gravitational waves in general. I then focus on GstLAL, one of the flagship pipelines of the
LVK collaboration to search for gravitational waves from compact binary coalescences. Alongside 
the overall structure of the GstLAL pipeline, I have also selected and explained several key terms 
in the likelihood ratio calculation for triggers in the GstLAL pipeline. With
proper modifications, these chosen terms will allow us to enhance the search sensitivity of GstLAL further. For instance,
in Chapter \ref{Chapter: gstlal_idq}, I introduced a method to effectively incorporate statistical
data quality information from the machine-learning-based pipeline iDQ \cite{Essick:2020qpo,Godwin:2020weu}
into GstLAL's likelihood ratio calculations. This method aims to (partially) resolve the problem of
current GstLAL noise background estimation, which does not track time dependence nor data quality.
Improvements to the other highlighted terms are left as future work (See the next section).

In the second part of my thesis, I switched my focus towards gravitational lensing of gravitational
waves. In Chapter \ref{Chapter: Intro_to_lensing}, I gave an overview of gravitational lensing in
general and gravitational lensing of gravitational waves. I also introduced current efforts made
by the LVK collaboration to search for lensing signatures in gravitational-wave data.
In Chapter \ref{Chapter: traditional_tesla}, I introduced the targeted sub-threshold search for strongly
lensed gravitational waves (TESLA) method. TESLA aims to search for possible sub-threshold (demagnified 
by gravitational lenses and hence with weaker amplitudes) strongly-lensed counterparts to confirmed
superthreshold gravitational waves. To reduce the noise background effectively while keeping the targeted
foreground constant, TELSA conducts injection campaigns to narrow the search parameter
space, accounting for both the target super-threshold event signal subspace and noise fluctuations in the data.
Through a simulation campaign, we have shown that TESLA can boost our search sensitivity towards possible
sub-threshold lensed gravitational waves in the data, if they exist. 
In Chapter \ref{Chapter: O3a_lensing}, \ref{Chapter: O3b_lensing} and \ref{Chapter: O3_lensing_followup},
we present results from the LVK collaboration-wide search for gravitationally-lensed gravitational waves
from the first-half and entire third observing run (O3). The TESLA analysis to search for possible sub-threshold
lensed gravitational waves was included as one of the key analyses.
While we have found no evidence of gravitational lensing in O3 data, we expect that with the detectors'
sensitivities improving, upcoming LVK's observing runs will continue to populate our catalog of gravitational-wave 
detections that will foster our study and understanding of gravitational lensing of gravitational waves. 
With the help of multi-messenger astronomy and improved analysis methods, we have high hopes of making 
the first (and many more) detection of gravitationally-lensed gravitational waves in the near future.
In Chapter \ref{Chapter: TESLA-X} and \ref{Chapter: TESLA-X-plus}, I further introduced two significant improvements
to the TESLA search method. Chapter \ref{Chapter: TESLA-X} describes how we can construct a better
reduced template bank and a targeted population model to improve our sensitivity towards possible
sub-threshold lensed gravitational waves. Chapter \ref{Chapter: TESLA-X-plus}
demonstrates how we can fine-tune the list of possible lensed gravitational-wave candidates reported 
by the TESLA search method by the incorporation of lens model information. These improvements are
crucial and critical to improve our chance of detecting the first gravitationally-lensed
gravitational waves.

\section{Future work}
A wise friend once told me, ``There is always room for improvement, and things are not done
all at once. They are completed in stages."
His words apply to both research work and science development. While I am ending my work as a PhD candidate
here, there is still a lot of work left to do to exhaust the potential of gravitational waves to fully
understand the Universe.

The TESLA-X+ pipeline can be further improved:

Firstly, the current TESLA-X method arbitrarily defines the region of interest in the search parameter space
as enclosed by the least significant contour of the Gaussian Kernel Density Estimation we
constructed using the recovered injections from the injection campaign. While we have shown that
TESLA-X can recover more lensed injections than the traditional TESLA method and attain a larger
sensitivity range, the current approach does discard certain templates that can recover lensed
injections, but are outside of the least significant contour of the Gaussian KDE constructed. In the future,
we will investigate ways to improve the construction of the targeted population model further. For instance, we may consider first ``flattening" the TESLA-X bank (by constructing a uniform mass model) before applying the targeted population model. We may also need to address the problem that the Gaussian KDE can exceed the physical boundary of the original template bank. 
%a possible method might be to include a uniform prior on top of the Gaussian KDE
%constructed based on the recovered injections. This will result in a more spread-out Gaussian KDE, which
%can help to re-include the currently discarded templates when constructing the reduced template bank.
%However, this will simultaneously increase the number of templates in the reduced template bank,
%resulting in higher trials factors, which may hurt the search sensitivity by increasing the
%noise background. The targeted population model would be an excellent moderator to balance between high trials factors
%due to having too many templates, and losing signals due to useful templates being discarded. 

Secondly, as discussed in Chapter \ref{Chapter: TESLA-X-plus}, additional injection campaigns are needed
to quantify the significance of the lensing likelihood values reported by the TESLA-X+ pipeline. Currently,
they can only be used as reference information because we lack a robust study to understand the distributions
of the lensing likelihoods of ``background", not-lensed gravitational waves, compared to that for truly
lensed gravitational waves. We will need to conduct more thorough background studies to unleash the full potential of the lensing likelihood. 

The lensing likelihood can also be extended to include sky location information 
of the triggers' sources. Because the uncertainty in source localization for gravitational waves 
is much larger than the deviation in source positions of lensed gravitational waves from the same source, 
we can assume lensed gravitational waves originating from the same source to have the same
sky localization. This can be potentially added as an additional parameter to be considered in the lensing
likelihood and enhance its power to distinguish possible lensed gravitational waves from not-lensed 
gravitational waves.

We can also work on the GstLAL search pipeline for more general sky-location-based targeted searches. 
We want to modify the GstLAL search pipeline to only search for gravitational waves from 
a specific region in the sky. The use case for sky-location-based 
targeted searches are not only constrained to searching for strongly-lensed gravitational waves. One can also
apply the same methodology to look for gravitational-wave counterparts to EM observations/detections. For
instance, a potential use case would be to conduct targeted Gamma-Ray Burst searches \cite{FermiGamma-RayBurstMonitorTeam:2023mtr,LIGOScientific:2021iyk} / Fast Radio Burst searches \cite{LIGOScientific:2022jpr} that look for gravitational-wave
counterparts to detected Gamma-Ray Bursts / Fast Radio Bursts. To do so, however, we must modify the likelihood
ratio calculation in the GstLAL search pipeline to include parameters that encode the sky localization of triggers.
Additionally, we must develop a (semi-analytic) signal and noise model to include the
sky-location-related parameters in the likelihood ratio.
Moreover, we will need a rapid method implemented in GstLAL to efficiently localize the source for
the many triggers reported by GstLAL. At the same time, we will also need to implement a better ``fast-path cut"
function to drastically reduce the number of triggers to be followed up without discarding triggers that may
be an actual gravitational-wave signal. Should such a ``fast-path cut" function be implemented,
we will be able to bypass the current clustering process in the GstLAL pipeline and enable the reranking
of triggers in the future, potentially removing the need to refiltering data due to the use of a subset of 
templates in the full template bank (e.g. the TESLA-X method), or the use of an alternative population model.

We are entering a golden age to pursue gravitational-wave science with multi-messenger astronomy and 
observational high-energy astrophysics, 
which requires the worldwide collaboration of the LVK collaboration and EM astronomers and telescopes. 
There is enormous potential to learn about the early universe through gravitational lensing and multi-messenger astronomy with gravitational waves.
The truth is out there for us to discover. . .


%\begin{figure}[hbt!]
%\centering
%\includegraphics[width=\textwidth]{cross_polarization.png}
%\caption{Cross Polarization.}\label{fig:cross_polarization}
%\index{figures}
%\end{figure}

