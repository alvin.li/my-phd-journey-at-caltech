\section{Introduction}
In this chapter, I will give an overview of the LIGO-Virgo-KAGRA (LVK) collaboration's framework
to search for gravitational waves. Some of the known sources of gravitational waves include:
(1) gravitational waves from compact binary coalescences (CBCs),
(2) gravitational-wave bursts that may originate from transients like supernovae,
(3) continuous gravitational waves from sources emitting gravitational waves continuously
at quasi-constant frequency, for example, spinning asymmetric neutron stars,
and (4) stochastic gravitational-wave background that consists of a collection of gravitational
waves that are not well-localized.
In this thesis, however, we will only be focusing on gravitational waves from compact binary
coalescences as LIGO has only confidently detected gravitational waves from CBCs at the time of writing. 

\section{Overview of current LVK's framework to search for gravitational waves from compact binary coalescences}
There are several key steps in current LVK's framework to search for and analyze gravitational waves 
from compact binary coalescences: 
(1) Calibrating detector data, 
(2) Detector characterization and noise mitigation, 
(3) Searching for significant possible gravitational-wave candidates, and
(4) Performing parameter estimation for possible gravitational-wave candidates.
%(5) High-latency follow-up analyses
%(Examples of high-latency follow-up analyses include testing general relativity \cite{DelPozzo:2014cla,Vallisneri:2012qq}
%and studies of gravitational-wave source population \cite{KAGRA:2021duu,Farr:2013yna,Liu:2020ufc}.
%In later Chapters, we will also discuss how we can search for gravitational lensing signatures in gravitational-wave data).

\subsection{\underline{Calibrating detector data}}
Before searching for possible gravitational-wave signals, 
the detector's data must first be calibrated and have their data quality checked, 
a process more commonly known as ``strain calibration''
\footnote{
	I will not pretend to be an expert 
	because I do not actively work on strain calibration.
	This will be something I am going to explore in the future. 
	Here, I will try my best to give a brief introduction.
}

When a typical gravitational wave passes through a detector, the detector arm lengths
will only change by a small amount $\Delta L$ on the order of $\mathcal{O}(10^{19})$m.
The corresponding gravitational wave strain is $h = \frac{\Delta L}{L}$, where $L$ is
the detector arm length (i.e. $4$km) (c.f. Equation \ref{eq: gw_strain} in Chapter \ref{Chapter: Introduction_to_DA_of_GW}).
To measure the change in arm lengths to that precision, 
the calibration of the detector data must also attain at least the same precision. 
We calibrate the detector data carefully using \textbf{feedback control loops} \cite{LIGOScientific:2016xax}.
\begin{figure}[hbt!]
\centering
\includegraphics[width=0.7\textwidth]{feedback_control_loops}
\caption{
	A block diagram of the feedback control loops used in the LIGO detectors. Figure taken from \cite{2019PASA...36...10T}.}
\label{fig:feedback_loops}
\index{figures}
\end{figure}
Feedback control loops (see Figure \ref{fig:feedback_loops}) are used to maintain the gravitational-wave detectors 
in a quasi-stationary state so that they can detect gravitational waves. 
One of these control loops is called the differential arm (DARM) length feedback loop \cite{LIGOScientific:2016xax}. 
The DARM length feedback loop, subdivided into sensing and actuation functions, 
is primarily used to keep the detector arm length difference steady. 
The sensing function transforms the differential displacement of the test mass to a digitized signal. 
This digitized signal represents the laser power fluctuation at the gravitational-wave readout port. 
The actuation function, on the other hand, can control the detector differential arm length by actuating 
on the quadruple suspension system for any of the arm cavity test masses. 
Details of the two functions can be found at \cite{LIGOScientific:2016xax}, which will not be repeated here.

For the Advanced LIGO detectors, we use photon calibrators, commonly known as ``Pcals'', 
as the primary calibration tool \cite{Karki:2016pht}. 
The working principle of photon calibrators is as follows: 
First, auxiliary, power-modulated laser beams are impinged onto a suspended test mass. 
Because of photon radiation pressure, as the test mass' mirror surface reflects the photons, 
the test mass will recoil accordingly. 
Since the laser beams are modulated, the forces on the test mass are periodic and are directly 
proportional to the amplitude of the laser power modulation, the test mass' position will hence 
undergo modulation together with the length of the detector arm cavity. 
Figure \ref{fig:pcal} illustrates how photon calibrators imping laser beams on a suspended test mass.
\begin{figure}[hbt!]
\centering
\includegraphics[width=0.5\textwidth]{Photon_calibrator}
\caption{
	An illustration of how photon calibrators imping laser beams on a suspended test mass. Figure taken from \cite{Karki:2016pht}.}
\label{fig:pcal}
\index{figures}
\end{figure}
We can also use the photon calibrators to produce tracked and known movements to the test masses.
This produces ``calibration lines'' at known frequencies to the detector data.
We can use the calibration lines to calibrate the output signal and track slow temporal variation \cite{Karki:2016pht}. 
Calibration lines are excitations induced using the photon calibrators at certain known 
nominal frequencies with known amplitudes (See Table III of \cite{Karki:2016pht}). 
The use of calibration lines allows one to apply corrections for slow temporal variations 
and hence improve the accuracy of data calibration. 
In Figure \ref{fig:psd_example}, two examples of calibration lines at frequencies $36.7$ Hz and $331.9$ Hz can be seen.

Interested readers are asked to refer to \cite{LIGOScientific:2016xax,Karki:2016pht} for more information about strain calibration.


\subsection{\underline{Detector characterization and mitigation}}
While Advanced LIGO detectors have employed various technologies to reduce noise from
non-astrophysical sources, the complicatedness of the detectors also introduces ``instrumental
noise'' to the data, affecting the data quality and our search sensitivity for gravitational waves.
It is, therefore, important to identify, diagnose, and mitigate detector noise that may affect
the quality of gravitational-wave data. These procedures are known collectively as \textbf{detector characterization} \cite{Davis:2022dnd}.

There are two major instrumental noise types: Transient and persistent noise.
Noise transients, more commonly known as ``glitches'', which are short-duration bursts of noise 
in the data. In contrast, persistent noise is long-term noise residing in the data. The ability
to correctly identify and characterize instrumental noise can help us to locate and possibly
remove known noise from the data (i.e. data mitigation), providing cleaner data for follow-up analyses to search for
and identify gravitational waves from the data. Detailed procedures for identifying and mitigating
instrumental noise that is characterize-able in the data can be found in \cite{Davis:2022dnd,LIGO:2021ppb}.

Because of the Advanced LIGO detectors' complexity and the wide range of possible noise sources,
not all noise in the data can be accurately characterized and mitigated. Data with quality
issues that cannot be mitigated will be ``vetoed'', and will not be analyzed by analysis pipelines. 
Identifying and vetoing problematic times of data is particularly important to searches for gravitational waves
from compact binary coalescences. This is because search pipelines must be capable of separating
bursts of excessive power caused by real gravitational-wave signals, from glitches caused by
instrumental noise \cite{Davis:2022dnd}. LIGO also employs pipelines to help identify times with 
high glitch rates. For example, Omicron \cite{Robinet:2020lbf} is used to identify and gather all
glitch times \cite{Davis:2022dnd}. A machine-learning-based pipeline, iDQ \cite{Essick:2020qpo,Godwin:2020weu},
is also used to find possible correlations with auxiliary witnesses in gravitational-wave data.
iDQ aims to automatically identify potential glitches in the gravitational-wave data and provide
information about the times in the data up to sub-second intervals likely to contain a glitch in low latency.
Search pipelines will incorporate information provided by the aforementioned tools about the data quality
and consider them when assigning ranking statistics to triggers found during times with
a high probability of corruption by glitches (See also \cite{Godwin:2020weu}). Later in Chapter \ref{Chapter: gstlal_idq},
we will introduce a proposed method to incorporate data quality information output from the iDQ pipeline
into the GstLAL search pipeline.

Since detector characterization is not the main focus of this thesis, readers are asked to refer to
\cite{Davis:2022dnd,LIGO:2021ppb} for more details.

\subsection{\underline{Searching for possible gravitational-wave signals from the data}}
Calibrated data are then ingested by ``search pipelines'' to search for possible candidates 
of gravitational waves in low-latency.
In this thesis, we are mainly focusing on the search for gravitational waves from 
compact binary coalescences. 
However, readers are reminded that other sources of gravitational waves are possibly 
detectable by ground-based gravitational-wave detectors, 
including continuous gravitational waves, gravitational-wave bursts, and stochastic gravitational-wave background.

Gravitational waves from compact binary coalescences have well-modelled waveforms that typically depend on
$15$ parameters.
\begin{figure}[hbt!]
\centering
\includegraphics[width=\textwidth]{gw_params}
\caption{
	The $15$ parameters that govern the waveforms of gravitational waves from compact binary coalescences.
}
\label{fig:gw_params}
\index{figures}
\end{figure}
\begin{itemize}
	\item{$m_1, m_2$: Masses of the two components,}
	\item{$\vec\chi_1, \vec\chi_2$: The three-dimensional (dimensionless) spins of the two components,}
	\item{$\iota$: Inclination of the orbital plane of the compact binary with respect to the line of sight,}
	\item{$\psi$: The orientation of the polarization vector with respect to the detector,}
	\item{$D_L$: Luminosity distance to the source,}
	\item{$\alpha, \delta$: The right ascension and declination of the compact binary,}
	\item{$t_c, \phi_c$: The coalescence time and phase.}
\end{itemize}
These parameters are also illustrated in Figure \ref{fig:gw_params}.
For gravitational waves from compact binaries that involve a neutron star, additional parameters known as the
``tidal deformability'' $\Lambda_i$, corresponding to effects related to the matter and internal structure of
neutron stars, are included. In some analyses, the eccentricity of the orbit of the compact binaries is also
included as one of the parameters.

There are two major types of search pipelines to search for possible gravitational waves from compact binary
coalescences, namely
(1) Modelled (matched-filtering-based) searches, and
(2) Unmodelled searches.

Modelled (matched-filtering-based) search pipelines, including GstLAL and pyCBC, 
rely on the fact that gravitational waves from compact binary coalescences have well-modeled waveforms. 
Given a region in the parameter space (e.g. in the component masses $m_1$-$m_2$ space) where gravitational
waves are within the frequency bands that LIGO detectors are sensitive to, 
we can precompute a set of gravitational waveforms 
(which we call ``template'' waveforms) within this region labelled by their respective parameters. 
This set of precomputed template waveforms is stored collectively in a ``template bank''. 
We then slide each of these templates across the data, and at each point, 
we compute the correlation between the template and data. 
The correlation amplitude is known as the signal-to-noise ratio (SNR). 
This is a process known as ``matched-filtering'' \cite{Allen:2005fk,1970PhT....23f..73H}. 
Suppose the SNR exceeds a certain pre-determined threshold at a given time instant. In that case, 
we record the time instant and label it as a possible candidate gravitational-wave signal (also known as a ``trigger'').
Modelled (matched-filtering-based) search pipelines will then rank these triggers according to some internally 
assigned ranking statistics from most likely to least likely to be an actual gravitational-wave signal. 
A ranked list of candidates is then released from the search pipeline for follow-up analysis. 
Details of matched-filtering-based pipelines will be further discussed in Chapter \ref{Chapter: introduction_gstlal}.

In contrast, unmodelled searches like the Coherent WaveBurst (CWB) pipeline \cite{Drago:2020kic} 
aims to identify gravitational waves with minimal assumptions on the signal morphology, 
i.e., there are no assumed waveform models. 
For example, the CWB pipeline looks for coherent (i.e., coincident in time) burst signals 
in the time-frequency domain for data across a network of detectors. 
Coherent triggers are subsequently assigned a likelihood ratio that compares the probability 
of the trigger associated with a gravitational-wave signal to that of the trigger originating from noise. 
If the trigger's likelihood ratio exceeds a predetermined threshold, 
it will be flagged by the pipeline as a candidate gravitational wave and passed on for follow-up analyses \cite{Klimenko:2008fu}.

While modelled and unmodelled searches have been used to search for gravitational waves, 
modelled searches, in general, are more effective in uncovering gravitational-wave signals. 
Some possible reasons are: 
(1) Modelled (matched-filtering-based) searches can maximize the signal-to-noise ratio (SNR) 
when the observed data is matched-filtered against a bank of template waveforms by design. 
If noise in the data is stationary and Gaussian, it has been shown that matched-filtered SNR 
is the optimal detection statistic to search for possible signals in the data \cite{Allen:2005fk}. 
(2) Unmodelled searches generally require signals to be seen in more than one detector (coherent signals). 
However, it is not uncommon for detectors in a detector network to be ``down'' 
(not in a state where it can collect scientific data) temporarily, 
leaving only a single detector collecting data. 
Alternatively, due to the geometry of the detectors, there exists ``blind spots'' in the sky 
at any given time from which gravitational waves are not detectable by a detector. 
In those scenarios, unmodelled searches that rely on the coherent detection of signals across 
detectors can potentially lose signals. 
Modelled (matched-filtering-based) searches, on the other hand, can find signals even if 
they are only detected in one detector
\footnote{
	This requires the signal to have a sufficiently high amplitude in general.
}.

In the rest of this thesis, we will focus on modelled (matched-filtering-based) search pipelines.

\subsection{\underline{Bayesian Parameter Estimation for significant events}}
As a next step, Bayesian parameter estimation is run on significant gravitational-wave candidates 
output from search pipelines to infer the sources' astrophysical properties 
(e.g. the source component masses and spins, c.f. Figure \ref{fig:gw_params}). 
Given the gravitational-wave data $d$ containing a gravitational-wave signal $s$, 
we want to infer the signal's source parameters $\vec\theta$. 
Formally, we want to determine the posterior $p(\vec\theta | d)$ \cite{2019PASA...36...10T}, 
i.e. the probability that the data contains a gravitational-wave signal with source parameters 
$\vec\theta$ given the data $d$. 
To evaluate this posterior, we make use of Bayes' theorem and write the posterior as
\begin{align}
	p(\vec\theta | d) = \frac{\mathcal{L}(d | \vec\theta) \pi(\vec\theta)}{\mathcal{Z}},
\end{align}
where $\mathcal{L}(d | \vec\theta)$ is the ``likelihood'' that gives the probability of obtaining 
the data $d$ if a gravitational-wave signal with source parameters $\vec\theta$ is present in the data;
$\pi(\vec\theta)$ is the ``prior'' distribution for the source parameters $\vec\theta$, and
\begin{align}
	\mathcal{Z} = \int \mathcal{L}(d | \vec\theta) \pi(\vec\theta) d\vec\theta
\end{align}
is the ``evidence''. In this case, the evidence is a normalization constant to the posterior.
However, it should be noted that if we are instead performing Bayesian model selection analysis,
the ``evidence'' $\mathcal{Z}$ will become important when evaluating the Bayesian odds \cite{2019PASA...36...10T}.
For instance, in Chapter \ref{Chapter: Intro_to_lensing}, \ref{Chapter: O3a_lensing}, \ref{Chapter: O3b_lensing}
and \ref{Chapter: O3_lensing_followup}, we perform Bayesian model selection analysis to determine how likely
a set of gravitational-wave signals are strongly-lensed counterparts to each other, originating from the same
source, as compared to being independent signals. In such analyses, the ``evidence'' is no longer
a mere normalization constant, but is crucial for evaluating the Bayesian odds ratio (See also \cite{Lo:2021nae}).

For most Bayesian parameter estimation analyses like Bilby \cite{Ashton:2018jfp},
a Gaussian noise likelihood, defined as
\begin{align}
	\mathcal{L} = \frac{1}{2\pi \sigma^2} \exp\left[- \frac{\left| d - h\left(\vec\theta\right) \right|^2}{2\sigma^2}\right],
\end{align}
where $h\left(\vec\theta\right)$ is a template waveform with parameters $\vec\theta$, $\sigma$
is the detector noise (assuming Gaussian noise), and $\pi$ is the usual mathematical constant (not the prior),
is adopted.

Once the prior and likelihood are set, 
one can adopt a stochastic sampler, e.g. Markov Chain Monte Carlo (MCMC) based samplers, 
to explore the parameter space and map out the posterior. 
Samplers employed by the Bilby parameter estimation pipeline \cite{Ashton:2018jfp} 
generate a list of posterior samples $\{\vec\theta\}$ drawn from the posterior probability distribution 
such that the number of samples is proportional to the posterior 
distribution $p(\vec\theta | d)$ on the interval $\left(\vec\theta, \vec\theta + d\vec\theta \right)$ \cite{2019PASA...36...10T}.

These posterior samples generated for each candidate gravitational-wave event, while
being a by-product of the parameter estimation analysis, are crucial
ingredients for other follow-up analyses to be conducted on the gravitational-wave events.
For instance, in Chapter \ref{Chapter: traditional_tesla}, posterior samples of confirmed
superthreshold gravitational-wave events are used to generate lensed injections for the
targeted search for possible sub-threshold lensed gravitational waves. 

Details on the exact Bayesian parameter estimation analysis can be found in \cite{2019PASA...36...10T,Ashton:2018jfp}.


%\begin{figure}[hbt!]
%\centering
%\includegraphics[width=\textwidth]{cross_polarization.png}
%\caption{Cross Polarization.}\label{fig:cross_polarization}
%\index{figures}
%\end{figure}

