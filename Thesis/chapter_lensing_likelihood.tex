%\fixme{Placeholder. Note that this chapter will be turned into a publication. Expect to complete this chapter in a few hours. It's not a difficult or long chapter to write anyway.}

\textbf{Note:} This chapter will be turned into a short author publication:
\begin{adaption}
	\begin{centering}
		\textbf{Alvin K.~Y.~Li} et al., ``TESLA-X+: Targeted search method for sub-threshold strongly lensed gravitational waves with lens-model-based ranking statistics'', In Preparation.
	\end{centering}
\end{adaption}


\section{Introduction}
The GstLAL-based TESLA / TESLA-X pipeline \cite{Li:2019osa,Li:2023zdl} has been one of the key pipelines for the LVK's collaboration 
to search method for possible sub-threshold lensed counterparts to known superthreshold gravitational waves \cite{LIGOScientific:2021izm,LIGOScientific:2023bwz}.
By doing injection campaigns, TESLA / TESLA-X narrows down the search parameter space and reduces the size of the template bank to
lower the noise background. With the help of a targeted population model, they help to improve the ranking statistics of 
possible sub-threshold lensed gravitational-wave candidates.
While the number of templates in targeted template banks generated through the TESLA-X pipeline is typically much lower than
that used in searches for gravitational waves in general
\footnote{
	For reference, the template bank used by the LVK collaboration in O3 to search for gravitational waves 
	has $\mathcal{O}(10^6)$ templates, whereas a typical TESLA-X targeted bank has $\mathcal{O}(10^3-10^4)$ templates.
},
targeted searches using the reduced template banks can still return $\mathcal{O}(10)$ of possible lensed candidates per
event. To do follow-up on these candidates to see how likely they are lensed counterparts to the target super-threshold
gravitational-wave event, one will first need to obtain the posterior probability distribution that gives the best 
estimates of the source parameters of the target using the Bayesian parameter estimation analyses described in~\cite{Veitch:2014wba, Ashton:2018jfp, Romero-Shaw:2020owr, Ashton:2021anp}.
For sub-threshold candidates that typically have lower signal-to-noise ratios (SNRs), conducting parameter estimation analyses
are extremely computationally expensive and time-consuming.
It is, therefore, vital to develop ways to discard candidates returned from each TESLA-X analysis effectively
that are unlikely lensed counterparts to the target event, before passing them for follow-up analysis. 

This chapter introduces a new ranking statistic for the TESLA-X pipeline incorporating lens model information.
This new ranking statistic, named ``lensing likelihood'', evaluates how likely a candidate returned from the TESLA-X pipeline
is a lensed counterpart to the target superthreshold event, assuming that the candidate is an actual gravitational-wave event.
It is important to note that the lensing likelihood is lens-model-based (i.e. its value can vary for different lens models).
The possible use cases of the lensing likelihood will be discussed later.

This chapter is organized as follows:
%Section \ref{Section: TESlA-X-Lensing} provides a brief review of strong lensing of gravitational waves. 
Section \ref{Section: TESLA-X+_lensing_theory} describes the motivation for implementing the lensing likelihood.
In Section \ref{Section: TESLA-X+_lensing_likelihood}, we describe how the lensing likelihood is constructed and evaluated.
Section \ref{Section: TESLA-X+_example} presents a working example of how the lensing likelihood can help to prioritize candidate-follow-up 
for the TESLA-X pipeline.
Finally, in Section \ref{Section: TESLA-X+_Conclusion}, we summarize our findings and discuss potential
future work to further enhance the usefulness of the newly implemented lensing likelihood.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Motivation and Background}
\label{Section: TESLA-X+_lensing_theory}

Strong gravitational lensing can produce copies of gravitational waves from the same source.
These copies have the same intrinsic parameters (hence the same waveform morphology) but with different
amplitudes (due to different amplification factors applied) and phases (due to different Morse phase
factors applied). Some of these copies are de-amplified compared to the not-lensed gravitational wave
because the amplification factor is $<1$, and may become ``sub-threshold''
\footnote{
	As explained in previous chapters, ``sub-threshold'' signals are low-amplitude signals that are
	indistinguishable from the noise background.
}.
GstLAL-based TESLA-X \cite{Li:2019osa,Li:2023zdl} (See also Chapter \ref{Chapter: TESLA-X}) has been 
the flagship LVK matched-filtering-based search pipeline to search for strongly-lensed sub-threshold lensed
gravitational-wave counterparts to confirmed superthreshold gravitational waves from LVK's first three observing
runs \cite{Hannuksela:2019kle,LIGOScientific:2021izm,LIGOScientific:2023bwz}.
In this section, we will give a brief review of the TESLA-X pipeline, and motivate the implementation of the
lensing likelihood, which will be discussed in the next section.


\subsection{\underline{The TESLA-X search pipeline: A brief recap}}
TESLA-X aims to search for possible sub-threshold lensed counterparts to
known superthreshold gravitational waves. These possible new candidates are not identified
as gravitational waves in the general search by the LVK collaboration because a large template
bank spanning a vast region in the search parameter space is used. As more templates are used
in a search, more candidates (triggers), whether they are real signals or noise, will be generated.
Because of the high trials factor, the noise background generated in a general search for gravitational
waves will be large, hence making it more difficult for low-amplitude signals, e.g. possible sub-threshold
lensed gravitational waves, to be distinguished from noise and be identified as signals.
TESLA-X strategically reduces the nuisance noise background while keeping the targeted foreground constant
to uncover possible gravitational waves with weaker amplitudes. 


For a given superthreshold gravitational-wave event, 
TESLA-X first considers the posterior probability distribution that provides the best estimates of
the source parameters of the target using BILBY, a Bayesian parameter estimation pipeline \cite{Veitch:2014wba, Ashton:2018jfp, Romero-Shaw:2020owr, Ashton:2021anp}.
Under the strong lensing hypothesis, we expect possible sub-threshold lensed counterparts to the target superthreshold
gravitational-wave event to have similar intrinsic parameters. Therefore, it is natural to consider localizing the
search parameter space to regions consistent with the target's posterior parameter space.
However, we note that the posterior distributions obtained for the target event are only valid for one noise realization.
Due to noise fluctuations in the data, weaker signals (near or sub-threshold) can be registered as triggers
by templates with parameters very different from those of the posterior samples of the target event \cite{Li:2019osa,Li:2023zdl}.

To account for both consistency in intrinsic parameters with the target event, and noise non-stationarity,
TESLA-X takes the posterior samples of a target superthreshold gravitational-wave event from parameter
estimation as input, and sorts them in decreasing order of log-likelihood. 
We aim to determine the region in the parameter space where weaker lensed counterparts can be recovered. 
These possible lower-amplitude signals will be registered as triggers in searches with lower signal-to-noise ratios $\rho$ (SNR).
For each sample, TESLA-X generates $1$ injection with identical parameters (including luminosity distance to the source $D_L$
and sky location, i.e. right ascension $\alpha$ and declination $\delta$) as the sample, and $9$ extra injections with increasing $D_L$ 
to mimic the effect of de-magnification due to strong lensing on the amplitudes of the possible sub-threshold signals, according to the
relation $D_L \propto \rho^{-1}$. 

The set of lensed injections is injected into real data. Then, an injection campaign is performed using GstLAL
with a general, full template bank to try to recover the injections (As before, an injection is recovered if the false-alarm-rate FAR of the associated trigger is $\leq$ $1$ in $30$ days.).
Recovered injections with their associated templates are then used to generate a Gaussian Kernel Density Estimation (KDE) $p(\vec\theta)$
on the search parameter space represented by $\vec\theta$
\footnote{
	$\vec\theta$ labels the intrinsic parameters of the templates in the search parameter space.
	They can be different sets of parameters, e.g. ``chirp mass $\mathcal{M}_c$ - effective spin $\chi_\text{eff}$''
	or ``component mass $1$ $m_1$ - component mass $2$ $m_2$'', depending on the region of parameter space 
	we are probing.
}
with the recovered SNR of the injections as weights. 
Templates that live within the lowest contour of the KDE are kept to construct a targeted template bank. The KDE
is further used to generate a targeted population model \cite{Heather,Li:2023zdl} that 
describes the expected distribution of possible (sub-threshold) lensed counterparts
to the target super-threshold gravitational wave. 

Finally, TESLA-X uses the GstLAL search pipeline again to search through all possible data with
the generated targeted template bank and population model. 
GstLAL will output a list of possible candidate events ranked according 
to their assigned ranking statistics, including the likelihood ratios $\mathcal{L}$ and
false-alarm-rates FARs. These ranking statistics assigned by GstLAL, however, only
represents how likely the candidate is an actual gravitational-wave event as compared to noise,
i.e. it does not include any additional information regarding how likely the candidate event
is a lensed counterpart to the target event, compared to being an independent event. 

Candidates that satisfy a pre-determined FAR threshold are then passed on for follow-up analyses,
which require parameter estimation to be first done on each new candidate. 
Since we expect to see many sub-threshold candidates with lower SNRs, it is, in general, more computationally
costly and time-consuming to conduct parameter estimation for them.

In the LVK's search for lensing signatures in full O3 data \cite{LIGOScientific:2023bwz}, the TESLA pipeline
reported $2372$ new sub-threshold candidates that passes the usual $1$ in $30$ days FAR threshold from targeted searches 
for $39$ superthreshold gravitational-wave events \cite{O3b_lensing_datarelease}. 
This corresponds to approximately $60$ new candidates per event.
In the paper, we set a stricter threshold on the FAR and only follow up on the most promising $\mathcal{O}(1)$ candidates
due to the expensive computational costs.
However, we note that the FAR assigned to each candidate only tells us how likely they are gravitational-wave events as
compared to noise.
Lensing information (i.e., how likely these candidates are lensed counterparts to the target event) has not been considered.
To better trim the list of candidates and only follow up on those that are more likely to be an actual gravitational-wave event, and
more likely to be lensed counterparts to the target event, 
we need a second alternative ranking statistic that accounts for the consistency of the candidates' parameters with the lensing hypothesis.
In the next section, We will give a detailed discussion about this new statistic, the lensing likelihood.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{The Lensing Likelihood}
\label{Section: TESLA-X+_lensing_likelihood}

In gravitational lensing, the primary observables are the relative time delays and
magnifications between the pair of lensed events \cite{More:2021kpb}.
Given a lens model, one can simulate lensed gravitational-wave events and model the expected distributions 
of the relative time delays $p(\Delta t | \mathrm{lensed})$ and magnifications (Magnification ratios) $p(\mu_\text{rel} | \mathrm{lensed})$ 
of the possible lensed event pairs. We will use the joint probability distribution $p(\Delta t, \mu_\text{rel} | \mathrm{lensed})$ instead of the factorized probability distribution.
We can repeat the exercise for independent (not lensed) gravitational-wave events, and obtain the joint probability distribution
of their relative time delays and magnifications $p(\Delta t, \mu_\text{rel} | \text{not lensed})$ \cite{More:2021kpb}. 
\begin{figure}[hbt!]
\centering
\includegraphics[width=\textwidth]{lensed_unlensed_distribution.png}
\caption{
	The Kernel Density Estimations of the distribution of log relative time delay and magnification ratios for the simulated lensed gravitational waves (green contours and pale blue sample points) for the SIE-double model and not-lensed, independent gravitational waves (black contours with grey sample points). We can see that the KDE for not-lensed, independent gravitational waves spans a larger range of relative magnifications and relative time delays solely because these gravitational-wave events are randomly distributed, in contrast to lensed gravitational waves under a given lens model.}
\label{Fig:lensed_and_unlensed_distribution}
\index{figures}
\end{figure}
Figure \ref{Fig:lensed_and_unlensed_distribution} shows an example of the Kernel Density Estimations of the distribution of log relative time delay and magnification ratios for the simulated lensed gravitational waves (green contours and pale blue sample points) for the SIE-double model and not-lensed, independent gravitational waves (black contours with grey sample points). We can see that the KDE for not-lensed, independent gravitational waves spans a larger range of relative magnifications and relative time delays solely because these gravitational-wave events are randomly distributed, in contrast to lensed gravitational waves under a given lens model. Note that here we are only considering galaxy-scale lenses that typically have lens masses ranging from $10^6 M_\odot$ to $10^{13} M_\odot$ (See, for example, Table \ref{tab:O3b_lensedrates0_min} in Chapter \ref{Chapter: O3b_lensing}). For galaxy-cluster-scale lenses with lens masses $\geq 10^{13} M_\odot$, lensed gravitational waves can have larger relative time delays (i.e. of the order of years), with larger absolute magnifications.

Since lensed and not lensed gravitational waves have different distributions for relative time delays and relative magnifications. one can construct the proposed ``lensing likelihood'' with these joint distributions as
\begin{align}
	\mathcal{L}^\text{lensing} = \frac{p(\Delta t, \mu_\text{rel} | \text{lensed, signal})}{p(\Delta t, \mu_\text{rel} | \text{not lensed, signal})}.
\end{align}
For candidates returned from the search pipeline, relative magnification can be defined as the ratio of the candidate's SNR to that of the target event.
Note that we have implicitly assumed that the candidates under consideration are real gravitational-wave signals.

In contrast to the likelihood assigned by search pipelines that evaluate how likely a candidate is a gravitational-wave signal as compared to noise,
the lensing likelihood evaluates how likely a candidate is a lensed counterpart to a given target event, as compared to being completely
independent events, by considering the relative time delay and magnification between the two events under consideration. The higher the lensing likelihood,
the more consistent the lensing observables (i.e. relative time delay and magnification) of the candidate are with the lens model under consideration, and thus, the
more likely the candidate is a lensed counterpart to the target event under consideration.

There are two caveats to the interpretation of the lensing likelihood: 
(1) The lensing likelihood assumes the candidate is an actual gravitational-wave event. 
This means that the lensing likelihood only assesses the consistency with the strong lensing hypothesis of the candidates' relative time delay $\Delta t$ and relative magnification $\mu_\text{rel}$ compared to the target event, regardless of whether the candidate is an actual signal or noise.
(2) The lensing likelihood heavily depends on the lens model under consideration. Different lens models have different expected distribution
for the relative time delays and magnifications between lensed signals, and hence, the lensing likelihood for the same candidate can change when
one varies from one lens model to another.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Working example}
\label{Section: TESLA-X+_example}

This section uses the mock lensed event MGW220111a introduced in \cite{Li:2019osa} as a working example for the lensing likelihood implementation in TESLA.
As a quick recap, a pair of lensed events (GPS time: $1325932493$ and $1326029051$), one superthreshold and one subthreshold,
is injected into a $28$-hour-long data stream with Gaussian noise recolored with O3a characteristic power spectral densities (PSDs).
The two signals are image type 1 and type 2 signals, respectively.
Detailed setup and parameters of the injected events can be found in Section IV of \cite{Li:2019osa}, and Section \ref{Section: TESLA_MDC} of Chapter \ref{Chapter: traditional_tesla}.

In the TESLA targeted search for the mock superthreshold event MGW220111a in \cite{Li:2019osa},
we can uprank the missing sub-threshold lensed counterpart (GPS time: $1326029051$) and retrieve
it as a rank-3 candidate, followed by the target event MGW220111a (rank-1 candidate) and a noise trigger (rank-2 candidate).
Table \ref{Table: MGW220111a Targeted search results top 3} shows the detailed information of the top three candidates from the TESLA targeted search.
\begin{table}[hbt!]
\centering
\begin{tabular}{c c c c}
	\hline
	\hline
	Search results & Superthreshold & Noise & Subthreshold \\
	& signal & trigger & signal\\
	\hline
	GPS time & $1325932493$ & $1326011224$ & $1326029051$\\
	$\log_{10}\Delta t$ (days) & - & $-0.04$ & $0.04$ \\
	Rank & $1$ & $2$ & $3$ \\
	FAR (Hz) & $5.37\times10^{-21}$ & $1.18\times10^{-5}$ & $4.27\times10^{-5}$ \\
	$\ln \mathcal{L}$ & $48.63$ & $13.53$ & $12.13$ \\
	Network SNR $\rho_\text{network}$ & $12.20$ & $7.12$ & $7.60$ \\
	$\log_{10}\mu_\text{rel}$ & - & $0.23$ & $0.21$ \\
	\hline
	\hline
\end{tabular}
\caption{
	\label{Table: MGW220111a Targeted search results top 3}
	Top three candidates from the TESLA targeted search for the mock lensed event MGW220111a.
}
\index{tables}
\end{table}
Should this situation be encountered in a real analysis, we will have to follow up on both 
the rank 2 and rank 3 triggers, because we do not know a priori whether they (1) correspond to
a real gravitational wave, and (2) are lensed counterparts to the target event (i.e. rank 1 trigger).

Following \cite{More:2021kpb}, we simulated lensed gravitational waves based on the singular isothermal
ellipsoid (SIE) lens model. The SIE lens model allows for systems of $2$ lensed images (also known as ``doubles'')
and systems of $4$ images (also known as ``quads''). For our example, we will only consider the 
SIE-double model. Based on the simulated lensed gravitational waves, we generate Gaussian Kernel Density 
Estimation (KDE) for the relative time delays $\log_{10} \Delta t$ (in days) and magnification ratios ($\log_{10}\mu_\text{rel}$)
between the lensed images. We repeat the exercise with simulated, not-lensed, independent gravitational waves.
Then, for both the rank 2 and rank 3 triggers listed in Table \ref{Table: MGW220111a Targeted search results top 3},
we evaluate the log lensing likelihood $\ln\mathcal{L}^\text{lensing}$. Table \ref{Table: Lensing_likelihood_result}
shows the evaluated results. In Figure \ref{Fig:lensing_likelihood_contour} we also plot the relative time delays 
and magnification ratios for the rank 2 (in red) and rank 3 (in blue) triggers, on top of the KDE distributions 
for lensed gravitational waves (green contours) and not-lensed, independent gravitational waves (filled contours). 
\begin{table}[hbt!]
\centering
\begin{tabular}{c c c}
	\hline
	\hline
	Log Lensing & Rank 2 & Rank 3 \\
	Likelihood & trigger & trigger \\
	$\ln\mathcal{L}^\text{lensing}$ & 3.80 & 3.98 \\
	\hline
	\hline
\end{tabular}
\caption{
	\label{Table: Lensing_likelihood_result}
	Log Lensing Likelihoods for the rank 2 and 3 triggers evaluated based on the SIE-double lens model.
}
\index{tables}
\end{table}
\begin{figure}[hbt!]
\centering
\includegraphics[width=\textwidth]{lensing_likelihood_contour.png}
\caption{
	The log relative time delay and magnification ratios of the rank 2 trigger (in red) and rank 3 trigger (in blue)
	respectively. The Kernel Density Estimations of the distribution of log relative time delay and magnification ratios
	for the simulated lensed gravitational waves (green contours) and not-lensed, independent gravitational waves (filled contours)
	are also plotted. We note that the rank 3 trigger, corresponding to the real sub-threshold lensed counterpart to MGW220111a,
	is slightly closer to the lensed KDE distribution than the rank 2 noise trigger, resulting in a slightly higher 
	log lensing likelihood.}
\label{Fig:lensing_likelihood_contour}
\index{figures}
\end{figure}
We can see that the lensing likelihood of the rank 3 trigger, corresponding to the real sub-threshold lensed
gravitational-wave counterpart to the mock superthreshold event MGW220111a, has a ``marginally-larger'' log 
lensing likelihood ($\ln \mathcal{L}^\text{lensing} = 3.98 > 3.80$) than that of the rank 2 trigger, corresponding
to a noise trigger. In Figure \ref{Fig:lensing_likelihood_contour}, we can see that the rank 3 trigger's (in blue) lensing
observables (relative time delay and magnification) is slightly closer to the lensed gravitational wave distribution
(green contours), compared to those of the rank 2 trigger (in red). 
While being a marginal case, this still demonstrates that the lensing likelihood has the potential to uprank candidates
that are more likely to be a lensed counterpart to the target event under consideration. However, it should be noted that
the lensing likelihood calculation is based on the assumption that the candidate under consideration is a real gravitational
wave. This is not the case in this example, where the rank 2 trigger is a noise trigger. In addition,
the lensing likelihood is highly dependent on the lens model under consideration, and may vary vastly should a different 
lens model be considered. Therefore, we remind readers that the lensing likelihood, at the current stage, should be treated 
with caution and only be used as reference information.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion and future work}
\label{Section: TESLA-X+_Conclusion}

TESLA-X has been the LVK collaboration's key pipeline to search for possible sub-threshold lensed counterparts to known 
superthreshold gravitational-wave events. As of the O3 analysis, the TESLA-X pipeline can return $60$, possibly
lensed sub-threshold candidates per superthreshold event. Due to the expensive computational costs, we trim down
the candidate list by imposing a high threshold on the false-alarm-rate FAR assigned to the candidates by the search
pipeline. However, as mentioned in \cite{Li:2019osa,Li:2023zdl}, the ranking statistics assigned by the search pipeline 
to each candidate only consider how likely the candidate is an actual gravitational-wave signal compared to noise. It contains
no information about how likely the candidate is a lensed counterpart to the target event under consideration. Therefore,
a pure cut in FAR to reduce the number of candidates to follow up with may potentially lead to real lensed gravitational 
waves being discarded. 

In this chapter, we introduce an alternative ranking statistic for the TESLA-X pipeline that incorporates lens model information.
The new ranking statistic, or ``lensing likelihood'', evaluates how likely a candidate returned from the TESLA-X pipeline
is a lensed counterpart to the target superthreshold event, assuming that the candidate is an actual gravitational-wave event.
The lensing likelihood takes in the relative time delay and magnification ratios between the sub-threshold lensed candidate and
the target event, and compare the probability of obtaining this set of observables under the hypothesis that they are lensed 
counterparts to each other, as compared to under the hypothesis that they are not-lensed, independent events.

We demonstrate how the lensing likelihood can potentially uprank real lensed sub-threshold gravitational waves with the mock gravitational-wave
event MGW220111a case as an example. However, we note the following caveats to the interpretation of the lensing likelihood: 
(1) The lensing likelihood is evaluated based on the assumption that the candidate is an actual gravitational-wave event. 
(2) The lensing likelihood depends heavily on the lens model under consideration. 

There are several possible use cases for the lensing likelihood proposed in this chapter. (1) The lensing likelihood can be used as a separate ranking statistic to uprank / downrank candidates, (2) We can also multiply the lensing likelihood to the search pipeline's likelihood that addresses how likely the candidate is a signal as compared to noise, then use the combined ranking statistic to uprank / downrank candidates. We may as well just use the new ranking statistic as a rank / label, but we can also run simulation campaigns to understand the distribution
of lensing likelihoods for lensed gravitational-wave events and independent gravitational-wave events to give a better statistical interpretation of the new ranking statistic values. In the future, we can also consider other lens models and different types of lenses (e.g. galaxies and galaxy clusters). Since the calculations are done after the searches as a post-processing step, the additional computational cost of considering more lens models and lens types will be minimal.

%\begin{figure}[hbt!]
%\centering
%\includegraphics[width=\textwidth]{cross_polarization.png}
%\caption{Cross Polarization.}\label{fig:cross_polarization}
%\index{figures}
%\end{figure}

