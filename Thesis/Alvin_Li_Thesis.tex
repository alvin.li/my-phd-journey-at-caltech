

%%%%%%%%%%%% 
%% Please rename this main-bibtex.tex file and the output PDF to
%% [lastname_firstname_graduationyear]
%% before submission.
%%
%% This .tex file is for use with BibTeX. Please use
%% main.tex instead if you prefer BibLaTeX.
%%%%%%%%%%%%

\documentclass[12pt]{caltech_thesis}
\usepackage{algpseudocode}
\usepackage[hyphens]{url}
\usepackage{lipsum}
\usepackage{xspace}
\usepackage{adjustbox}
\usepackage{changepage}
\usepackage{caption}
\usepackage[autostyle=true]{csquotes}
\usepackage{parallel}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{CJKutf8}
\usepackage{hyperref}
\graphicspath{{figures/}}
\usepackage{todonotes}
\usepackage{tensor}
\usepackage{lscape}
\newcommand*{\fixme}[1]{{\color{red} [\textbf{FIXME: }#1]}}

%% Tentative: newtx for better-looking Times
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{newtxtext,newtxmath}
\usepackage{tikz}
\usepackage[most]{tcolorbox}
\newtcolorbox{adaption}{enhanced, interior hidden, frame code={
  \draw[rounded corners] (interior.north west)++ (0,-0.5) -- ++(0,0.5) -- ++(0.5,0) ;
  \draw[rounded corners] (interior.south east)++ (0,0.5) -- ++(0,-0.5) -- ++(-0.5,0) ;
}}

\usepackage[nolist,nohyperlinks,printonlyused]{acronym}
\usepackage{appendix}
\AtBeginEnvironment{subappendices}{%
\chapter*{Appendix}
\addcontentsline{toc}{chapter}{Appendices}
\counterwithin{figure}{section}
\counterwithin{table}{section}
}

\usepackage[numbers,sort&compress]{natbib}
\usepackage{bibunits}
\defaultbibliographystyle{plainnat}
\usepackage{enumitem}
\newcommand{\gstlal}{\texttt{GstLAL }}
\newcommand{\pycbc}{\textsc{PyCBC}\xspace}
\setcounter{secnumdepth}{5}


\begin{document}

\counterwithin{figure}{section}
\counterwithin{table}{section}

% Do remember to remove the square bracket!
\title{
	Probing the higher redshift universe by studying strong lensing of gravitational waves and
	enhancing search sensitivity of the GstLAL search pipeline
}
\author{Alvin Ka Yue Li}

\degreeaward{Doctor of Philosophy in Physics}                 % Degree to be awarded
\university{California Institute of Technology}    % Institution name
\address{Pasadena, California}                     % Institution address
\unilogo{caltech.png}                                 % Institution logo
\copyyear{2024}  % Year (of graduation) on diploma
\defenddate{May 3, 2024}          % Date of defense

\orcid{[0000-0001-6728-6523]}

%% IMPORTANT: Select ONE of the rights statement below.
\rightsstatement{All rights reserved}
% \rightsstatement{All rights reserved except where otherwise noted}
% \rightsstatement{Some rights reserved. This thesis is distributed under a [name license, e.g., ``Creative Commons Attribution-NonCommercial-ShareAlike License'']}

%%  If you'd like to remove the Caltech logo from your title page, simply remove the ''[logo]'' text from the maketitle command
\maketitle[logo]
%\maketitle

\begin{acknowledgements}
	\input{acknowledgement}
   %[Add acknowledgements here. If you do not wish to add any to your thesis, you may simply add a blank titled Acknowledgements page.]
\end{acknowledgements}

\begin{abstract}
	The LIGO-Virgo-KAGRA (LVK) collaboration first observed gravitational waves in 2015, 
	and more than $90$ gravitational-wave events have been observed, 
	all coming from mergers of compact objects (black holes and neutron stars), 
	known as compact binary coalescences (CBC). 
	Studying and observing gravitational waves opens a new window for us to understand the nature 
	of spacetime and the universe. 
	Strain data from LVK's detectors are analyzed by search pipelines to identify weak gravitational-wave
	signals in noisy data.
	To maximize the potential of gravitational waves, 
	it is essential to continue to improve search pipelines' sensitivity to probe GW sources with the 
	broadest range of parameters and from the furthest distances. 
	I will give a detailed overview of the GstLAL pipeline and present related development 
	(ongoing) work for GstLAL to enhance its search effectiveness and efficiency.

	In the second part of my thesis, 
	I will focus on gravitational lensing of gravitational waves. 
	As masses can produce curvature in spacetime, gravitational waves, 
	like electromagnetic (EM) waves, are deflected when passing by massive intervening objects before 
	reaching gravitational-wave detectors on Earth, 
	an effect known as gravitational lensing. 
	Observing lensed gravitational waves confirms another prediction in Einstein's general relativity and 
	enables us to conduct cosmography studies, test general relativity, search for dark matter and other exotic phenomena, 
	and deepen our understanding of the universe. 
	I will give a detailed introduction to gravitational lensing of gravitational waves. 
	We then introduce a Targeted subthreshold search for strongly-lensed gravitational wave pipeline called ``TESLA''. 
	The TESLA pipeline is the flagship to look for sub-threshold lensed gravitational waves. 
	Next, we present the results of the LVK collaboration-wide effort to search for lensing signatures in 
	gravitational-wave data from the third observing run O3. 
	Next, we introduce a significant update to the TESLA pipeline, now known as the TESLA-X pipeline, with enhanced search sensitivity 
	towards lensed gravitational waves. 
	We also introduce an alternative ranking statistic implemented into the TESLA-X pipeline that considers 
	the signal's consistency with the assumed lens model. 
	Finally, we end the thesis with a summary and an outline of possible future work.
\end{abstract}

\extrachapter{Published Content and Contributions}

% List your publications and contributions here.
% The list will be formatted according to \defaultbibliographystyle in the preamble. Use the ''note'' field instead of the ''addendum'' field to describe author's role.

\bigskip

\begin{description}[leftmargin=2em,labelindent=0pt,labelsep=0pt]
\SingleSpacing

\item[[1]] \textbf{Alvin K.~Y.~Li} et al., ``Targeted subthreshold search for strongly lensed gravitational-wave events'', \emph{Phys. Rev. D} 107.12 (2023), \textsc{doi}: \texttt{10.1103/PhysRevD.107.123014}.\\
(\textbf{Alvin~K~.Y.~Li} is the first author of this paper. He is the main developer of the ``TargetEd Subthreshold Lensing seArch'' (TESLA) method to find possible sub-threshold lensed counterparts to known gravitational waves described in this manuscript and led the writing of the manuscript.)

\item[[2]] \textbf{Alvin K.~Y.~Li} et al., ``TESLA-X: An effective method to search for sub-threshold lensed gravitational waves with a targeted population model”, \emph{arXiv e-prints}, 2023, \textsc{doi}: \texttt{arXiv.2311.06416}.\\
(\textbf{Alvin~K~.Y.~Li} is the lead author of this paper, which describes the next generation “TargetEd Subthreshold Lensing seArch” (TESLA) method with the use of better-constructed reduced template bank and a targeted population model.)

\item[[3]] Y.~Wang, R.~K.~L.~Lo, \textbf{Alvin K.~Y.~Li} and Y.~B.~Chen, ``Identifying Type II Strongly Lensed Gravitational-Wave Images in Third-Generation Gravitational-Wave Detectors''. \emph{Phys. Rev. D} 103.10 (2021). \textsc{doi}: \texttt{10.1103/PhysRevD.103.104055}.\\
(\textbf{Alvin K.~Y.~Li} led the mock simulation campaign to search for type II lensed gravitational waves with a type I image template bank using TESLA, as described in section III in the paper. He also edited Section III of the paper.)

\item[[4]] LIGO Scientific and Virgo Collaborations, R.~Abbott, ...(723 authors)..., \textbf{Alvin K.~Y.~Li}, ...(655 authors)..., ``Search for Lensing Signatures in the Gravitational-Wave Observations from the First Half of LIGO–Virgo's Third Observing Run'', \emph{Astrophys. J.} 923.1 (2021), \textsc{doi}: \texttt{10.3847/1538-4357/ac23db}.\\
(\textbf{Alvin K.~Y.~Li} led the analysis for the search for sub-threshold lensed gravitational waves and edited the corresponding section in the LIGO-Virgo-KAGRA collaboration-wide paper (Section 5.3).)

\item[[5]] LIGO and Virgo Scientific Collaboration, R.~Abbott, ...(850 authors)..., \textbf{Alvin K. Y. Li}, ...(842 authors)..., ``Search for gravitational-lensing signatures in the full third observing run of the LIGO-Virgo network'', \emph{arXiv e-prints}, 2023. \textsc{doi}:\texttt{10.48550/arXiv.2304.08393}.\\
(\textbf{Alvin K.~Y.~Li} is one of the editorial leads for writing this LIGO-Virgo-KAGRA collaboration-wide paper and led the analysis for the search for sub-threshold lensed gravitational waves (Section 3.1).)

\item[[6]] J.~Janquart, ...(6 authors)..., \textbf{Alvin K.~Y.~Li}, ...(22 authors)..., ``Follow-up Analyses to the O3 LIGO-Virgo-KAGRA Lensing Searches'', \emph{Mon. Not. Roy. Astron. Soc.} 526.3 (2023), \textsc{doi}:\texttt{10.1093/mnras/stad2909}.\\
(\textbf{Alvin K.~Y.~Li} led the analysis and provided results for the search for GstLAL-based sub-threshold lensed gravitational waves described in the paper. He also helped with editing the corresponding sections in the paper.)

\item[[7]] S.~Goyal, S.~Kapadia, J.~R.~Cudell, \textbf{Alvin K.~Y.~Li}, and J.~C.~L.~Chan, ``A rapid method for preliminary identification of subthreshold strongly lensed counterparts to superthreshold gravitational-wave events'', \emph{Phys. Rev. D} 109.2 (2024), \textsc{doi}:\texttt{10.1103/PhysRevD.109.023028}.\\
(\textbf{Alvin K.~Y.~Li} led the analysis and provided results for the search for GstLAL-based sub-threshold lensed gravitational waves for follow-up analysis, as described in the paper. He also helped with editing the corresponding sections in the paper.)

\item[[8]] B.~Ewing, ...(7 authors)..., \textbf{Alvin K.~Y.~Li}, ...(35 authors)..., ``Performance of the low-latency GstLAL inspiral search towards LIGO, Virgo, and KAGRA's fourth observing run'', \emph{arXiv e-prints}, 2023, \textsc{doi}:\texttt{10.48550/arXiv.2305.05625}.\\
(\textbf{Alvin K.~Y.~Li} helped to develop, operate, and monitor the mock data challenge with GstLAL as described in the paper.)

\item[[9]] \textbf{Alvin K.~Y.~Li} et al., ``TESLA-X+: Targeted search method for sub-threshold strongly lensed gravitational waves with lens-model-based ranking statistics'', In Preparation.

\end{description}

%\begin{publishedcontent}[iknowwhattodo]
%\nocite{*}
%\putbib[ownpubs]
%\nocite{Li:2023zdl}
%\putbib[ownpubs-bibtex]
%\bibliography{ownpubs-bibtex}
%\end{publishedcontent}

%Note that it's not possible to get an existing BibTeX style to sort in reverse chronological order without modifying the \texttt{.bst} file. If you want a reverse-ordered list, the easiest thing may be to do it manually with a \texttt{description} list; an example is given in the comments of the source code below.

% \bigskip

% \begin{description}[leftmargin=2em,labelindent=0pt,labelsep=0pt]
% \SingleSpacing
% \item[] Cahn, J.~K.~B., A.~Baumschlager, et al. (2016). ``Mutations in adenine-binding pockets enhance catalytic properties of NAD (P) H-dependent enzymes''. In: \emph{Protein Engineering Design and Selection} 19.1, pp.~31--38. \textsc{doi}: \texttt{10.1093/ protein/gzv057}.

% \item[] Cahn, J.~K.~B., S. Brinkmann-Chen, et al. (2015). ``Cofactor specificity motifs and the induced fit mechanism in class I ketol-acid reductoisomerases''. In: \emph{Biochemical Journal} 468.3, pp.~475--484. \textsc{doi}: \texttt{10.1042/BJ20150183}.
% \\J.K.B.C participated in the conception of the project, solved and analyzed the crystal structures, prepared the data, and participated in the writing of the manuscript.
% \end{description}

\tableofcontents
\listoffigures
\listoftables
\printnomenclature

\mainmatter

\part{Introduction to gravitational waves and gravitational-wave data analysis with the GstLAL Search pipeline}

\chapter{Overview of the thesis}
\counterwithin{figure}{section}
\counterwithin{table}{section}
\input{chapter_front_matter}

\chapter{Introduction to gravitational waves}
\label{Chapter: Intro_to_GW}
\counterwithin{figure}{section}
\counterwithin{table}{section}
\input{chapter_introduction_to_gravitational_waves}

\chapter{Data analysis of gravitational waves from compact binary coalescences - An overview}
\label{Chapter: Introduction_to_DA_of_GW}
\counterwithin{figure}{section}
\counterwithin{table}{section}
\input{chapter_data_analysis_of_gw_from_cbc_an_overview}

%\part{Work and developments related to the GstLAL search pipeline}

\chapter{The GstLAL search pipeline - An overview}
\label{Chapter: introduction_gstlal}
\counterwithin{figure}{section}
\counterwithin{table}{section}
\input{chapter_gstlal_search_pipeline}

%\chapter{O4a Online GstLAL analysis}
%\counterwithin{figure}{section}
%\counterwithin{table}{section}
%\input{chapter_gstlal_o4a_analysis}

\chapter{Improving the ranking statistic assignment of the GstLAL search pipeline with the inclusion of iDQ data-quality information}
\label{Chapter: gstlal_idq}
\counterwithin{figure}{section}
\counterwithin{table}{section}
\input{chapter_idq_info}

%\chapter{Past, Present and Future miscellaneous development work for the GstLAL search pipeline}
%\label{Chapter: gstlal_sky_localization}
%\counterwithin{figure}{section}
%\counterwithin{table}{section}
%\input{chapter_sky_localization_gstlal}

% Section III%
\part{Gravitational Lensing of Gravitational Waves}

\chapter{Introduction to gravitational lensing and lensing of gravitational waves}
\label{Chapter: Intro_to_lensing}
\counterwithin{figure}{section}
\counterwithin{table}{section}
\input{chapter_lensing_of_gw}

\chapter{The Targeted subthreshold search for strongly lensed GWs (TESLA)}
\label{Chapter: traditional_tesla}
\counterwithin{figure}{section}
\counterwithin{table}{section}
\input{chapter_tesla_traditional}

%\chapter{Seven times the charm: The seven referee reports that help us to explain why TESLA is really effective}
%\counterwithin{figure}{section}
%\counterwithin{table}{section}
%\input{chapter_the_back_and_forth_of_tesla_journal_submission}

\chapter{Search for lensing signatures in the first half of LVK's third observing run}
\label{Chapter: O3a_lensing}
\counterwithin{figure}{section}
\counterwithin{table}{section}
\input{chapter_o3a_lensing_paper}

\chapter{Search for lensing signatures in LVK's full third observing run}
\label{Chapter: O3b_lensing}
\counterwithin{figure}{section}
\counterwithin{table}{section}
\input{chapter_full_o3_lensing_paper}

\chapter{Follow-up Analyses to the O3 LIGO-Virgo-KAGRA Lensing Searches}
\label{Chapter: O3_lensing_followup}
\counterwithin{figure}{section}
\counterwithin{table}{section}
\input{chapter_follow_up_lensing_o3}

\chapter{TESLA-X: A more effective targeted search method for sub-threshold strongly lensed gravitational waves}
\label{Chapter: TESLA-X}
\counterwithin{figure}{section}
\counterwithin{table}{section}
\input{chapter_tesla_x}

\chapter{Re-ranking possible sub-threshold lensed candidates with lensing likelihoods}
\label{Chapter: TESLA-X-plus}
\counterwithin{figure}{section}
\counterwithin{table}{section}
\input{chapter_lensing_likelihood}

%\chapter{Future developments for the TESLA method}
%\label{Chapter: Future-TESLA}
%\fixme{A lot of stuff - Wave optics? Higher order modes? Better ways to construct the reduced template bank? Reranking only instead of full re-filtering?}

\part{Summary}

\chapter{Summary and future work}
\label{Chapter: summary}
\counterwithin{figure}{section}
\counterwithin{table}{section}
\input{chapter_summary_and_future_work}


%Start off all chapters with \verb|chapter|. \index{chapter!numbered} \verb|\extrachapter| will give you an unnumbered chapter that's added to the Table of Contents. \index{chapter!unnumbered}

%Here's an example of a citation \citep{GMP81}. Here's another \citep{PP98}. These will appear in the big bibliography at the end of the thesis.
\index{bibliography}

%If you're new to \LaTeX{} and would like to begin by learning the basics, please see our free online course available at:\\ \url{https://www.overleaf.com/latex/learn/free-online-introduction-to-latex-part-1} \index{LaTeX@\LaTeX}

%You can define nomenclatures \index{nomenclature} as you talk about key terms in your thesis. So what's a galaxy? \nomenclature{Galaxy}{A system of stars independent from all other systems}


%\begin{figure}[hbt!]
%\centering
%\includegraphics[width=.3\textwidth]{caltech.png}
%\caption{This is a figure}\label{fig:logo}
%\index{figures}
%\end{figure}

%\subsection{This is a subsection}

%\begin{table}[hbt!]
%\centering
%\begin{tabular}{ll}
%\hline
%Area & Count\\
%\hline
%North & 100\\
%South & 200\\
%East & 80\\
%West & 140\\
%\hline
%\end{tabular}
%\caption{This is a table}\label{tab:sample}
%\index{tables}
%\end{table}


%Here's an endnote.\endnote{Endnotes are notes that you can use to explain text in a document.}



% Use bibunit if using bibtex/natbib
%\begin{bibunit}  
%If you'd like to have separate bibliographies at the end of each chapter, put a \verb|bibunit| around the material of each chapter, passing in the bibliography style to use, then cite as usual -- e.g.~\citep{GMP81,Ful83}. Then do a \verb|\putbib| (with the .bib file to refer to) just before the \verb|bibunit| ends. \index{bibliography!by chapter}
%
%\renewcommand{\bibsection}{\section*{\refname}}
%\putbib[example]  % the .bib file for this chapter
%\end{bibunit}

%\chapter{This is the Third Chapter}

%% If using bibtex (as opposed to biblatex), \publishedas takes TWO arguments
%\publishedas{ownpubs}{Cahn:etal:2016}

% OR you can format this paragraph manually if you have too much trouble with the above command.
% \begin{description}[leftmargin=2em,labelindent=0pt,labelsep=0pt]
% \SingleSpacing
% \item[] Cahn, J.~K.~B., S. Brinkmann-Chen, et al. (2015). ``Cofactor specificity motifs and the induced fit mechanism in class I ketol-acid reductoisomerases''. In: \emph{Biochemical Journal} 468.3, pp.~475--484. \textsc{doi}: \texttt{10.1042/BJ20150183}.
% \end{description}


%[You can have chapters that were published as part of your thesis. The text style of the body should be single column, as it was submitted to the publisher, not formatted as the publisher did.]

\bibliographystyle{plainnat}
\bibliography{example}

\appendix

%\chapter{Questionnaire}
%\chapter{Consent Form}

%\printindex

%\theendnotes

%% Pocket materials at the VERY END of thesis
%\pocketmaterial
%\extrachapter{Pocket Material: Map of Case Study Solar Systems}

\newpage
\mbox{}
\vfill
Before I end my thesis, I would like to include the last part of the song ``A Winter Story''.

\begin{Parallel}{0.48\textwidth}{0.48\textwidth}
	\ParallelLText{
	\begin{CJK}{UTF8}{min}
		あなたの全てが\\
		形を無くしても\\
		永遠に僕の中で生きてくよ\\
		さようなら出来ずに\\
		歩き出す僕と\\
		ずっと一緒に
	\end{CJK}
	}
	\ParallelRText{
		Even though your everything has lost its shape,
		you will still live inside me eternally.
		Although I was unable to say goodbye,
		I will now move on and continue with my life,
		for I know you will be with me forever.
	}
\end{Parallel}

Dear mother, this thesis and my thesis defense mark the end of my PhD journey and the end 
of a chapter in my life. While I will never be able to speak to you again, you will continue
to live inside my heart forever. I promise I will never forget about you, but for now, I am
going to move on to the next stage of my life. May you rest in peace.
\end{document}


