# Introduction

* Hi everyone! I'm Alvin. Who you see below is me, back in 2018, as a LIGO SURF student.

<img src="KaYueAlvinLi.png" alt="This is me" width="200"/>


This page serves to keep track of my PhD journey at Caltech. You will find here,
* The graduate requirements (and my progress),
* My current (and previous) works, and talks / presentations related to them
* My draft thesis (it will be far from complete, but many a little makes a mickle)
* My resume
* Other stuff that I think would make me / other people smile. :)

* What I want to learn (Version 1)
![What I want to learn](mindmap_what_I_want_to_learn.png)

# Caltech Graduate Requirements ([Link](http://pma.caltech.edu/research-and-academics/physics/physics-graduate-studies/requirements-for-a-doctorate-in-physics#WCE))
Here I quote and list the PhD requirements for Caltech Physics graduate students.

| Milestone | Timeframe | Expected date of completion | Done? |
| ------ | ------ | ------ | ------ |
| Submit Plan of Study for approval by Graduate Option Rep | By end of first term | - | :white_check_mark: |
| Complete 2 terms of Phys 242 Course | Fall & Winter Term of first year | - | :white_check_mark: |
| Written Candidacy Exam I: CM & EM | By the end of second year | Fall term, 2020-21 | :white_check_mark: |
| Written Candidacy Exam II: QM, Thermo and Stat Mech | By the end of second year | Fall term, 2020-21 | :white_check_mark: |
| Complete the Advanced Physics Requirement | By end of second year | | :white_check_mark: |
| Complete the Oral Candidacy Exam | By end of third year | 17 June 2022 | :white_check_mark: |
| Hold Annual Thesis Advisory Committee meetings | Annually after passing oral exam | | :x: |
| Final PhD Defense | By the end of fifth or sixth year | By March / April 2024 | :x: |

## Plan of study
Here, I list the courses I have taken / audited will be taking, and how they are related
to my plan of study and the advanced physics requirement.

| Course | Title | Term Taken | In plan of stduy? | Advanced physics requirement? | Completed? | 
| ------ | ------ | ------ | ------ | ------ | ------ |
| Ph129A | Mathematical Methods of Physics (I) | Fall, 2019-20 | Yes | Yes | :white_check_mark: |
| Ph129B | Mathematical Methods of Physics (II) | Winter, 2019-20 | Yes | Yes | :white_check_mark: |
| Ph129C | Mathematical Methods of Physics (III) | Spring, 2019-20 | Yes | Yes | :white_check_mark: |
| Ph236A | Relativity (I) | Fall, 2019-20 | Yes | Yes | :white_check_mark: |
| Ph236B | Relativity (II) | Winter, 2019-20 | Yes | Yes | :white_check_mark: |
| Ph237 | Gravitational Waves | Spring, 2019-20 | No | No | :white_check_mark: |
| Ph242A | Physics Seminar (I) | Fall, 2019-20 | Yes | No | :white_check_mark: |
| Ay121 | Radiative Processes | Fall, 2020-21 | Yes | Yes | :white_check_mark: |
| Ph139 | Introduction to Particle Physics | Winter, 2020-21 | No | No | :white_check_mark: |
| Ay125 | High Energy Physics | Spring, 2021-22 | No | No | :white_check_mark: |

You can find a temporary timetable for my G1-G3 year of studies at [link](https://drive.google.com/file/d/1ZWxUXU4XvfdTHwf4OOFJJ9LKacHRwxrz/view?usp=sharing) (Note that items may be subjected to changes).

# Work
## A. Targeted sub-threshold search for strongly lensed gravitational waves
We attempt to search for strongly-lensed counterparts of confirmed gravitational-wave events by using a reduced template bank, built through injection runs on simulated lensed injections created from posterior samples of the targeted events.

### A.1 Traditional TESLA search method
Here is the [methods paper](https://arxiv.org/abs/1904.06020) describing in details the whole idea and working principle. The paper has been accepted to the American Physical Society Physical Review D (APS PRD) journal for publication.

### A.2 Application of TESLA to O3a + b data
The [O3 pipeline](https://git.ligo.org/alvin.li/lensing-subthreshold-search-pipeline) is now being reviewed, and has been deployed to generate results for the collaboraiton-wide [O3a lensing paper](https://arxiv.org/abs/2105.06384) and [O3a+b lensing paper](https://arxiv.org/abs/2105.06384).

### A.3 FAST TESLA using targeted population model
Currently, we are upgrading the search method in terms of efficiency. We attempt to create a targeted population model based on the injection run results for each targeted event, and use that to rerank the full bank search results to skip the re-filtering process. A related talk can be found as the third item in the following list.

### A.4 Sky-overlap constraint included search
While lensing produces multiple images with different image positions, the deviation in positions for lensed gravitational waves is way smaller than the uncertainty arise from estimating the gravitational wave's source position, hence we can basically assume that lensed gravitational waves from the same source will have the same sky location. Based on this assumption, we can place a more stringent constraint while during the targeted search, and hence further improves its sensitivity.
A naive approach will be to assign a 0/1 factor to the ranking statistics of each found triggers, i.e. if the skymap overlap is 0, then we multiply the likelihood ratio by 0. Otherwise, we multiply it by 1 (i.e. no effect). Aidan Chong is currently investigating the possibliity of implementing a overlap-percentage-based factor (intermediate values between 0 and 1) to the ranking statistics.

### A.5 Approximation method for the targeted population model
The most computationally costly portion of TESLA comes from the injection run for each targeted event. We attempt to look for alternative ways to estimate / approximate the targeted population model without doing the injection runs. One way is to look at the SNR distribution of the rung-up templates by the original targeted event during the full-bank search run, and use that as a basis for generating the targeted population model. The accuracy of this method is yet to be investigated.

### A.6 Inclusion of lens-model information into the ranking statistics calculation
Different lens models predict different relative time delay and magnification distribution for pairs of lensed gravitational waves. In the gstLAL search we know the arrival times and SNRs of each triggers. This means that in principle we can use the information to further rank candidates based on their consistencies with the lens model under consideration. This is to be implemented by LIGO SURF 2023 student Haley Boswell.

Here is a list of dcc links to related talks:
* [APS April Meeting 2021](https://meetings.aps.org/Meeting/APR21/Session/X16.8), "Targeted Sub-threshold Search for Strongly-lensed Gravitational-wave Events"
* [APS April Meeting 2022](https://meetings.aps.org/Meeting/APR22/Session/Y15.3), "Latest results from the search for lensing signatures in gravitational-wave observations"
* [APS April Meeting 2023](https://meetings.aps.org/Meeting/APR23/Session/V09.3), "An effective method to search for sub-threshold lensed gravitational waves with a targeted population model"

## B. 

## B. Gravitational wave lensing mock data challenge 2022
As a test to the current LVK lensing framework, we design a mock data challenge which consists of a total of six months of data similar to the expected O4 scenario (similar expected noise background and number of events), with various lensed events included. The objective of the challenge is to see if the LVK lensing detection framework can confidently confirm lensing / not to falsely claim lensing.

A detailed summary of the MDC can be found [here](https://git.ligo.org/alvin.li/gravitation-wave-lol/-/wikis/Gravitational-Wave-Lensing-Mock-Data-Challenge-2022).

## C. Various work on the GstLAL pipeline
### 1. Early warning related work
See this [google document](https://docs.google.com/document/d/1-jZjHMR-BW5JORpFVJac-arPEL7m_XlGy48TlosuDck/edit) for more details. An overview will be provided here soon.

### 2. Better method for noise background estimation

### 3. Follow-up tools development in parallel with GWcelery.

### 4. Fine tuninig the online analysis pipeline.

# Thesis draft
In the sub-directory called *Thesis* under this main directory, you can find a draft thesis for my PhD studies. Currently is more or less just the template, but over the time I will update it. 

# Teaching duties
1. Ph106a Topics in Classical Physics (I), teaching assistant, Fall 2019
2. Ph106a Topics in Classical Physics (I), teaching assistant, Fall 2019


# Resume
You can find my most up-to-date resume [here](https://drive.google.com/file/d/1I9JDJGxdfSKVOyF-C8-Ivy36GW9IYLkR/view?usp=sharing).



