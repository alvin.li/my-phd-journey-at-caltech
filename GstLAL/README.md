# Project "Understanding GstLAL"

In the coming years I will probably work closely with the GstLAL pipeline, so I decided to devote some time into understanding every step (and functions) within the GstLAL pipeline.

## Functions
1. `gstlal_asd_txt_from_psd_xml`
> This code generates the asd from a LIGO PSD XML doc.

* Main code
```
for filename in filenames:
        for instrument, psd in lalseries.read_psd_xmldoc(ligolw_utils.load_filename(filename, verbose = options.verbose, contenthandler = lalseries.PSDContentHandler)).items():
                filename = "%s%s.txt" % (options.prefix, instrument)
                with open(filename, "w") as f:
                        if options.verbose:
                                print >>sys.stderr, "writing \"%s\" ..." % filename
                        for i, x in enumerate(psd.data.data):
                                print >>f, "%.16g %.16g" % (psd.f0 + i * psd.deltaF, x**.5)
```

* Key learning:
（a) `lalseries.read_psd_xmldoc`
> This LAL function reads in a dictionary of PSD frequency series from a LIGO XML doc

* Example (CIT:/home/alvin.li/understanding_gstLAL/gstlal_functions) :
```
gstlal_asd_txt_from_psd_xml --verbose --prefix O3a_chunk1_ H1L1V1-REFERENCE_PSD-1238166018-621936.xml.gz
```

2. `gstlal_bank_splitter`
> This code splits the main template bank into several sub-banks which are suitable for SVD operation. The templates are defaulted to be sort by ___chirp mass___. For each of the sub-bank, one can further group the templates by ___chi___ (N). If invoked, one will need to specify the ___numbanks___ M (number of parallel sub-banks per SVD bank), such that the bank will be splitted into N groups with M banks each.

When one is running the usual gstLAL analysis dag, one may have seen the error message `There are too few templates in this chi interval.  Requested %d: have %d" % (options.n, len(rows))`. This can be referred to the following lines of code.
```
for numrow, rows in enumerate(group_templates(chirows, options.n, options.overlap)):
                assert len(rows) >= options.n/2, "There are too few templates in this chi interval.  Requested %d: have %d" % (options.n, len(rows))
                # Pad the first group with an extra overlap / 2 templates
                if numrow == 0:
                        rows = rows[:options.overlap/2] + rows
                outputrows.append((rows[0], rows))
        # Pad the last group with an extra overlap / 2 templates
        outputrows[-1] = (rows[0], rows + rows[-options.overlap/2:])
```
To solve the problem, we usually reduce the number of n (i.e. group-by-chi), such that the option assertion is satisfied.

* Key learning:
(a) `ligolw_process.register_to_xmldoc`
In a LIGO XML doc, there is usually a table named `process` which documents all the previous work done within the document. This is done through the line of code `ligolw_process.register_to_xmldoc`. For example,
```
process = ligolw_process.register_to_xmldoc(xmldoc, program = "gstlal_bank_splitter", paramdict = options.__dict__, comment = "Split bank into smaller banks for SVD")
```
`ligolw_process.register_to_xmldoc` takes several arguments : ___xmldoc___ (the XML document), ___program___ (the name of the program), ___paramdict___ (a dictionary of name/value pairs that will be used to populate the table), ___comment___ (to comment on the process).

For example regarding ___paramdict___:
```
list(process_params_from_dict({"verbose": True, "window": 4.0, "include": ["/tmp", "/var/tmp"]}))
output: [(u'--window', u'real_8', 4.0), (u'--verbose', None, None), (u'--include', u'lstring', '/tmp'), (u'--include', u'lstring', '/var/tmp')]
```
Note that for a list, a tuple is produced for each item in the list.

* Example (CIT:/home/alvin.li/understanding_gstLAL/gstlal_functions) :
```
gstlal_bank_splitter --f-low 15.0 --group-by-chi 1 --output-path H1_split_bank_0 --approximant 0.00:1.73:TaylorF2 --approximant 1.73:1000.0:SEOBNRv4_ROM --output-cache H1_split_bank_1.cache --overlap 50 --instrument H1 --n 334 --sort-by mchirp --max-f-final 1024.0 --write-svd-caches --num-banks 3 --verbose bank_bbh_low_q-lensed_S190503bf_corrected_2.xml.gz
```
Here, I am using the S190503bf reduced template bank as an example. You will notice that I am only grouping the split bank templates into __1__ group of chi. The reason for this choice is that any number larger than __1__ will lead to the error message depicted above. Basically this means that our reduced template bank is a really small bank such that the chi range for the templates is really narrow, making it hard to split it further into smaller chi-intervals. 

In the left figure below, I plotted the templates in the reduced template bank (green "+") and the splitted bank (orange "."). The red "o" indicates repeated templates in the splitted bank. There are exactly __50__ repeated templates in the splitted bank, corresponding to the option __--overlap 50__ we invoked in the code. The overlap is to ensure we minimize the so-called __edge effect__.

<p float="left">
  <img src="/GstLAL/S190503bf_reduced_bank_splitting.png" width="550" />
  <img src="/GstLAL/S190503bf_reduced_bank_splitting_overlap_100.png" width="550" />
</p>

To ensure I am right about the option __--overlap__, I repeat the above code but with the option changed to __--overlap 100__, and then plot the result again. In the right figure above, the blue markers represent templates in the splitted bank, and pink dots represent repeated templates. Rest assured that there are exactly __100__ repeated templates in the splitted bank.

- This leads me to another thought - Is the overlap we are using right now the optimal value? What should be the overlap for our reduced template bank? We should investigate this a little bit more.

As another example, I split the low_q_bbh bank used in O3a using the usual options:
```
gstlal_bank_splitter --f-low 15.0 --group-by-chi 20 --output-path H1_split_bank_0 --approximant 0.00:1.73:TaylorF2 --approximant 1.73:1000.0:SEOBNRv4_ROM --output-cache H1_split_bank_1.cache --overlap 50 --instrument H1 --n 334 --sort-by mchirp --max-f-final 1024.0 --write-svd-caches --num-banks 3 --verbose bank_bbh_low_q.xml.gz
```
The bank contains a total __534914__ templates, with __334__ templates in each sub-bank, it is splitted into __1600__ sub-banks (534914 / 1600 ~ 334). 

For illustrative purpose, I first plot the templates in split_bank_0000 in the left figure below (note that I have focused only at the region for the split bank. The original bank in fact spans a much larger area in the mass space). Again, the orange dots represent templates in split_bank_0000, and the pink dots represent repeated templates in the split bank. Here, I notice that the number of repeated templates in this bank is only __25__ (instead of __50__ as we have invoked in the option __--overlap__). My first thought is that this is because we are at the marginal bank, so we will only worry about the "left" boundary of this bank. To verify this thought, I plot also the split_bank_0001 in the right figure below.

<p float="left">
  <img src="/GstLAL/low_q_bbh_split_bank_0000.png" width="550" />
  <img src="/GstLAL/low_q_bbh_split_bank_0000_0001.png" width="550" />
</p>

However, to my surprise, the number of repeated templates in the split_bank_0001 is still __25__. But then we should quickly realised that the second bank is also at the margin. If that's the case, we should go further and plot the bank somewhere in the middle to see what's going on. So I picked split_bank_0799 and plot it out.

BTW, just for a sanity check, I combined the templates in split_bank_0000 and split_bank_0001, and confirmed that there are no templates crossing over both banks, meaning that the templates in each sub-bank are "unique" to each other. 

<p float="left">
  <img src="/GstLAL/low_q_bbh_split_bank_0799.png" width="550" />
  <img src="/GstLAL/low_q_bbh_split_bank_0799_1000.png" width="550" />
</p>

But again, the results come in as another surprise - This time the splitted bank contains __NO__ repeated templates at all. Perhaps this means that only the sub-banks in the margin will have repeated templates. To verify this, let's try to plot another split bnak somewhere in the middle. For illustrative purpose, I pick the split_bank_1000. 

And of course, the split_bank_1000 also contains __NO__ repeated templates. Now we note that the two split banks look like "banana" in shape. This is because we have invoked the option __--sort-by mchirp__. Templates within the same banana have roughly the same chirp mass. 

<p float="left">
  <img src="/GstLAL/low_q_bbh_split_bank_1598_1599.png" width="550" />
</p>

Now, as a final step to verify that repeated templates occur only for banks near the margins, we plot the split banks 1598 and 1599 and see if there are any repeated templates. And the result is positive - Both banks have __25__ repeated templates, but there are no overlapping templates between the two banks.



